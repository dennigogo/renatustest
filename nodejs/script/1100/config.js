exports.config = {
    mongodb: {
        'url': 'mongodb://localhost:27017/tracker',
        'db': 'tracker'
    },
    port: 1100
}