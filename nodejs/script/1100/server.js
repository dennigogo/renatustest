#!/usr/bin/env node

const { config } = require('./config.js');

var express = require('express');
var router = express.Router();
var app = express();
var http = require('http');
var entry = require('./modules/entry/index');

http.createServer(app).listen(config.port);
app.use(
    '/event',
    router.get('/', entry)
);