"use strict";

const mongoDb = require('./../../../modules/mongodb/index.js');

/**
 *
 * @type {{OK: string}}
 */
const actionsMsg = require('./../../../modules/i18n/messages.js');

/**
 * @author Denis Gubenko
 */
class InsertAction {

    /**
     * @return {void}
     */
    constructor() {
        this.map = null;
    }

    /**
     *
     * @param maping
     */
    init(maping) {
        this.map = (typeof maping === 'object') ? maping : new Object();
        mongoDb.init(this.map);
    }

    /**
     *
     * @return {mixed}
     */
    insert() {
        let returned = false;
        let insertObject = new Object();
        let date = new Date();
        let datetime = (Date.UTC(
                date.getFullYear(), date.getMonth(), date.getDate(),
                date.getHours(), date.getMinutes(), date.getSeconds(),
                date.getMilliseconds()
        )/1000 + "").split(".");
        insertObject.type = this.map.type();
        insertObject.uid = this.map.uid();
        insertObject.tt = this.map.tt();
        insertObject.ts = this.map.ts();
        insertObject.createdFull = parseFloat(datetime.join("."));
        insertObject.created = parseInt(datetime[0]);
        insertObject.ms = parseInt(datetime[1]);
        if (false !== mongoDb.insertOne('info', insertObject)) {
            returned = actionsMsg.OK;
        }

        return returned;
    }

}

/**
 *
 * @type {InsertAction}
 */
let insertAction = new InsertAction();

/**
 *
 * @param map
 */
exports.init = (map) => insertAction.init(map);

/**
 *
 * @return {boolean}
 */
exports.insert = () => insertAction.insert();