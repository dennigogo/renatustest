"use strict";

const insertAction = require('./../../modules/actions/insert/index.js');

/**
 * @author Denis Gubenko
 */
class Actions {

    /**
     * @return {void}
     */
    constructor() {
        this.map = null;
        this.request = null;
        this.response = null;
    }

    /**
     *
     * @param maping
     * @param req
     * @param res
     */
    init(maping, req, res) {
        this.map = (typeof maping === 'object') ? maping : new Object();
        this.request = (typeof req === 'object' && req.constructor.name === 'IncomingMessage') ? req : null;
        this.response = (typeof res === 'object' && res.constructor.name === 'ServerResponse') ? res : null;
        insertAction.init(this.map);
    }

    /**
     *
     * @return {mixed}
     */
    actionsResponse() {
        let returned = false;
        if (
            null !== this.response && null !== this.request
            && false !== this.checkType()
            && false !== this.checkUid()
            && false !== this.checkTt()
            && false !== this.checkTs()
        ) {
            returned = insertAction.insert();
        } else {
            returned = false;
        }

        return returned;
    }

    /**
     *
     * @return {boolean}
     */
    checkType() {

        return (null !== this.map.type());
    }

    /**
     *
     * @return {boolean}
     */
    checkUid() {
        return (null !== this.map.uid());
    }

    /**
     *
     * @return {boolean}
     */
    checkTt() {
        return (null !== this.map.tt());
    }

    /**
     *
     * @return {boolean}
     */
    checkTs() {
        return (null !== this.map.ts());
    }
}

/**
 *
 * @type {Actions}
 */
let actionsClass = new Actions();

/**
 *
 * @param map
 * @param req
 * @param res
 */
exports.init = (map, req, res) => actionsClass.init(map, req, res);

exports.response = () => actionsClass.actionsResponse();