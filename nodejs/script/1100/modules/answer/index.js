"use strict";

const errors = require('./../../modules/errors/index.js');
const actions = require('./../../modules/actions/index.js');

/**
 * @author Denis Gubenko
 */
class Answer {

    /**
     * @return {void}
     */
    constructor() {
        this.map = null;
        this.request = null;
        this.response = null;
        this.answerCode = 200;
        this.contentType = {'content-type': 'application/json'};
    }

    /**
     *
     * @param maping
     * @param req
     * @param res
     */
    init(maping, req, res) {
        this.map = (typeof maping === 'object') ? maping : new Object();
        this.request = (typeof req === 'object' && req.constructor.name === 'IncomingMessage') ? req : null;
        this.response = (typeof res === 'object' && res.constructor.name === 'ServerResponse') ? res : null;
        this.initErrorClass(maping, req, res);
        this.initActionsClass(maping, req, res);
    }

    /**
     * @return {void}
     */
    initErrorClass(maping, req, res) {
        errors.init(maping, req, res);
    }

    /**
     * @return {void}
     */
    initActionsClass(maping, req, res) {
        actions.init(maping, req, res);
    }

    sendResponseMsg() {
        let messageObj = new Object();
        let result = actions.response();
        if (false !== result) {
            messageObj.status = 'OK';
            messageObj.result = result;
            this.response.writeHead(this.answerCode, this.contentType);
            this.response.end(JSON.stringify(messageObj));
        }
    }

    /**
     * @return {void}
     */
    sendErrorsMsg() {
        errors.error();
    }
}

/**
 *
 * @type {Answer}
 */
let answer = new Answer();

/**
 *
 * @param map
 * @param req
 * @param res
 */
exports.init = (map, req, res) => answer.init(map, req, res);

exports.response = () => answer.sendResponseMsg();
exports.error = () => answer.sendErrorsMsg();