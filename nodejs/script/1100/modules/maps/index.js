"use strict";

const { config } = require('./../../config.js');

/**
 * @author Denis Gubenko
 */
class ValidationQuery {

    /**
     *
     * @return {string[]}
     */
    queryParams() {
        let queries = [
            'type',
            'uid',
            'ts',
            'tt'
        ];

        return queries;
    }

    /**
     *
     * @param key
     * @param value
     * @return {boolean}
     */
    isValid(key, value) {
        let returned = false;
        let queries = this.queryParams();
        if (
            typeof key === 'string' && 0 < key.length
            && typeof value === 'string' && 0 < value.length
            && false !== queries.indexOf(key)
        ) {
            switch (key) {
                case queries[0] :
                    returned = this.testType(value);
                    break;
                case queries[1] :
                    returned = this.testUid(value);
                    break;
                case queries[2] :
                    returned = this.testTs(value);
                    break;
                case queries[3] :
                    returned = this.testTt(value);
                    break;
            }
        }

        return returned;
    }

    /**
     *
     * @param input
     * @return {boolean}
     */
    testType(input) {

        return (null !== input.match(/^(install|login|end)$/)) ? true : false;
    }

    /**
     *
     * @param input
     * @return {boolean}
     */
    testUid(input) {

        return (null !== input.match(/^([a-zA-Z]+)$/)) ? true : false;
    }

    /**
     *
     * @param input
     * @return {boolean}
     */
    testTs(input) {

        return (null !== input.match(/^([a-z_]+)$/)) ? true : false;
    }

    /**
     *
     * @param input
     * @return {boolean}
     */
    testTt(input) {

        return (null !== input.match(/^([a-z_]+)$/)) ? true : false;
    }
}

/**
 * @author Denis Gubenko
 */
class QueryMap {

    /**
     *
     * @type {ValidationQuery}
     */
    validationClass() {
        if (typeof this.validationQuery !== 'object' || this.validationQuery.name !==  'ValidationQuery') {
            this.validationQuery = new ValidationQuery();
        }

        return this.validationQuery;
    }

    /**
     *
     * @param key
     * @param value
     */
    router(key, value) {
        let validation = this.validationClass();
        let queryParams = validation.queryParams();
        switch (key) {
            case queryParams[0]:
                this.setType(value);
                break;
            case queryParams[1]:
                this.setUid(value);
                break;
            case queryParams[2]:
                this.setTs(value);
                break;
            case queryParams[3]:
                this.setTt(value);
                break;
        }
    }

    /**
     * @return {void}
     */
    reset() {
        this.setType();
        this.setUid();
        this.setTt();
        this.setTs();
    }

    /**
     * @return string
     */
    getType() {
        let returned = null;
        if (typeof this.type == 'string' && 0 < this.type.length) {
            returned = this.type;
        }

        return returned;
    }

    /**
     *
     * @param string
     */
    setType(input) {
        let validation = this.validationClass();
        this.type = null;
        if (typeof input == 'string' && 0 < input.length && validation.isValid('type', input)) {
            this.type = input;
        }
    }

    /**
     * @return string
     */
    getUid() {
        let returned = null;
        if (typeof this.uid == 'string' && 0 < this.uid.length) {
            returned = this.uid;
        }

        return returned;
    }

    /**
     *
     * @param string
     */
    setUid(input) {
        let validation = this.validationClass();
        this.uid = null;
        if (typeof input == 'string' && 0 < input.length && validation.isValid('uid', input)) {
            this.uid = input;
        }
    }

    /**
     * @return string
     */
    getTt() {
        let returned = null;
        if (typeof this.tt == 'string' && 0 < this.tt.length) {
            returned = this.tt;
        }

        return returned;
    }

    /**
     *
     * @param string
     */
    setTt(input) {
        let validation = this.validationClass();
        this.tt = null;
        if (typeof input == 'string' && 0 < input.length && validation.isValid('tt', input)) {
            this.tt = input;
        }
    }

    /**
     * @return string
     */
    getTs() {
        let returned = null;
        if (typeof this.ts == 'string' && 0 < this.ts.length) {
            returned = this.ts;
        }

        return returned;
    }

    /**
     *
     * @param string
     */
    setTs(input) {
        let validation = this.validationClass();
        this.ts = null;
        if (typeof input == 'string' && 0 < input.length && validation.isValid('ts', input)) {
            this.ts = input;
        }
    }
}

/**
 *
 * @type {QueryMap}
 */
let queryMap = new QueryMap();

/**
 *
 * @type {ValidationQuery}
 */
let validationQuery = new ValidationQuery();

/**
 *
 * @type {{reset: function(): void, router: function(*=, *=): void, type: function(): string, uid: function(): string, ts: function(): string, tt: function(): string}}
 */
exports.map = {
    'config': config,
    'reset': () => queryMap.reset(),
    'router': (key, value) => queryMap.router(key, value),
    'type': () => queryMap.getType(),
    'uid': () => queryMap.getUid(),
    'ts': () => queryMap.getTs(),
    'tt': () => queryMap.getTt()
}

/**
 *
 * @return {string[]}
 */
exports.queries = () => validationQuery.queryParams();