"use strict";

/**
 *
 * @type {{type: string, uid: string, tt: string, ts: string, server: string}}
 */
const errorsMsg = require('./../../modules/i18n/errors.js');

/**
 * @author Denis Gubenko
 */
class Errors {

    /**
     * @return {void}
     */
    constructor() {
        this.map = null;
        this.request = null;
        this.response = null;
        this.errorCode = 400;
        this.contentTypeError = {'content-type': 'application/json'};
    }

    /**
     *
     * @param maping
     * @param req
     * @param res
     */
    init(maping, req, res) {
        this.map = (typeof maping === 'object') ? maping : new Object();
        this.request = (typeof req === 'object' && req.constructor.name === 'IncomingMessage') ? req : null;
        this.response = (typeof res === 'object' && res.constructor.name === 'ServerResponse') ? res : null;
    }

    /**
     * @return {void}
     */
    error() {
        let errors = new Object();
        let messageObj = new Object();
        if (false === this.checkType()) {
            errors.type = errorsMsg.type;
        }
        if (false === this.checkUid()) {
            errors.uid = errorsMsg.uid;
        }
        if (false === this.checkTt()) {
            errors.tt = errorsMsg.tt;
        }
        if (false === this.checkTs()) {
            errors.ts = errorsMsg.ts;
        }
        if (0 == errors.length) {
            errors.server = errorsMsg.server;
        }
        if (null !== this.response && null !== this.request) {
            messageObj.status = 'ERROR';
            messageObj.errors = errors;
            this.response.writeHead(this.errorCode, this.contentTypeError);
            this.response.end(JSON.stringify(messageObj));
        }
    }

    /**
     *
     * @return {boolean}
     */
    checkType() {

        return (null !== this.map.type());
    }

    /**
     *
     * @return {boolean}
     */
    checkUid() {
        return (null !== this.map.uid());
    }

    /**
     *
     * @return {boolean}
     */
    checkTt() {
        return (null !== this.map.tt());
    }

    /**
     *
     * @return {boolean}
     */
    checkTs() {
        return (null !== this.map.ts());
    }
}

/**
 *
 * @type {Errors}
 */
let errorsClass = new Errors();

/**
 *
 * @param map
 * @param req
 * @param res
 */
exports.init = (map, req, res) => errorsClass.init(map, req, res);

exports.error = () => errorsClass.error();