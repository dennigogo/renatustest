"use strict";

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

var mongoDbConn = null;
var promiseOpen = [];

/**
 * @author Denis Gubenko
 */
class MongoDbClass {

    /**
     * @return {void}
     */
    constructor() {
        this.map = null;
    }

    /**
     *
     * @param maping
     */
    init(maping) {
        this.map = (typeof maping === 'object') ? maping : new Object();
        promiseOpen = [];
        promiseOpen.push(() => this.open());
    }

    open() {
        let url = this.map.config.mongodb.url;
        return new Promise((resolve, reject)=>{
            // Use connect method to connect to the Server
            MongoClient.connect(url, (err, db) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(db);
                }
            });
        });
    }

    /**
     *
     * @param collection
     * @param object
     * @return {Promise}
     */
    insertOne(collection, object) {
        let url = this.map.config.mongodb.url;
        let db = this.map.config.mongodb.db;
        let input = {
            collection: collection,
            object: object
        }
        return new Promise((resolve, reject)=> {
            MongoClient.connect(this.map.config.mongodb.url, function (err, client) {
                assert.equal(null, err);
                client.db(db).collection(input.collection).insertOne(input.object, function (err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        client.close();
                        resolve(db);
                    }

                });
            });
        });
    }

    /**
     *
     * @param collection
     * @param object
     * @return {boolean}
     */
    insertItem(collection, object) {
        let returned = false;
        promiseOpen = [];
        promiseOpen.push((collection, object) => mongoDbClass.insertOne(collection, object));
        for (const value of promiseOpen) {
            value(collection, object);
            returned = true;
        }

        return returned;
    }
}

/**
 *
 * @type {MongoDbClass}
 */
let mongoDbClass = new MongoDbClass();

/**
 *
 * @param map
 */
exports.init = (map) => mongoDbClass.init(map);

/**
 * @
 * @param collection
 * @param object
 * @return {boolean}
 */
exports.insertOne = (collection, object) => mongoDbClass.insertItem(collection, object);