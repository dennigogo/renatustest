var express = require('express');
var entry = express.Router();
const { map, queries } = require('./../../modules/maps/index.js');
const { EntryRouter } = require('./../../modules/router/index.js');

entry.get('/', function(req, res) {
    let error = false;
    let params = queries();
    map.reset();
    if (typeof req.query !== 'undefined') {
        let inputObj = Object.keys(req.query);
        if (0 < inputObj.length) {
            inputObj.forEach(function (key) {
                if (false !== params.indexOf(key)) {
                    map.router(key, req.query[key]);
                }
            });
        }
    }
    let entryRouter = new EntryRouter(map);
    entryRouter.activeRouter().response(req, res);
})

module.exports = entry;