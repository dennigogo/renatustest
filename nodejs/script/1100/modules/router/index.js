"use strict";

const { init, error, response } = require('./../../modules/answer/index.js');

/**
 * @author Denis Gubenko
 */
class EntryRouter {

    /**
     *
     * @param input
     */
    constructor(input) {
        this.map = (typeof input === 'object') ? input : new Object();
        this.activeRouter(this.map);
    }

    /**
     *
     * @return {ActiveRouter}
     */
    activeRouter(input) {
        if (
            typeof this.router !== 'object' || this.router.constructor.name !== 'ActiveRouter'
        ) {
            this.router = new ActiveRouter(input);
        }

        return this.router;
    }
}

/**
 * @author Denis Gubenko
 */
class ActiveRouter {

    /**
     *
     * @param input
     */
    constructor(input) {
        this.map = (typeof input === 'object') ? input : new Object();
    }

    /**
     *
     * @param req
     * @param res
     * @return {boolean}
     */
    response(req, res) {
        let returned = false;
        if (
            typeof res === 'object' && res.constructor.name === 'ServerResponse'
            && typeof req === 'object' && req.constructor.name === 'IncomingMessage'
        ) {
            init(this.map, req, res);
            if (false !== this.check()) {
                response();
            } else {
                error();
            }
            returned = true;
        }

        return returned;
    }

    /**
     *
     * @return {boolean}
     */
    check() {
        let result = (
            null !== this.map.type() && null !== this.map.uid()
            && null !== this.map.tt() && null !== this.map.ts()
        );

        return result;
    }
}

/**
 *
 * @type {{EntryRouter: EntryRouter}}
 */
module.exports = {
    EntryRouter
};