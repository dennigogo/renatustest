use tracker;
var n = new Array();
db.info.aggregate([
    {
        $match:{
            type:{$in:["login","end"]},
            created:{
                $gt:(1*((new Date("2018-05-06T00:00:00Z"))*1/1000 + '.000')),
                $lt:(1*((new Date("2018-05-06T23:59:59Z"))*1/1000 + '.999'))
            }
        }
    },
    {
        $project:{_id:0,uid:1,type:1,tt:1,ts:1,created:1}
    },
    {
        $sort:{_id:1}
    },
    {
        $group:{_id:"$uid", actions:{$push:"$$ROOT"}}
    }
]).forEach(function(i){i.actions.forEach(function(z){
    var g = db.info.aggregate([{$match:{type:"install",uid:z.uid,tt:z.tt}},{$project:{_id:1,uid:1,type:1,tt:1,ts:1,created:1}},{$sort:{_id:1}}]).toArray().length;
    if (0 == g) {
        n.push(z);
    }
})});
n;
