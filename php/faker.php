<?php

require_once './protected/include.php';

use RenatUsTest\Modules\Faker\InitMongoCollectionInfo;

$faker = new InitMongoCollectionInfo();
$addDays = (!empty($argv[1]) && is_numeric($argv[1])) ? (int) $argv[1] : null;

$faker->populateInfo('info', $addDays);