<?php

namespace RenatUsTest\Throws;

/**
 * Class ErrorExceptions
 * @package RenatUsTest\Throws
 * @author Denis Gubenko
 */
class ErrorExceptions extends \ErrorException implements \Throwable
{

	public function __construct()
    {
		@set_exception_handler([ $this, 'exception_handler' ]);
	}

    /**
     * @param $exception
     */
	public function exception_handler($exception)
    {
		http_response_code(404);
		echo "Warning!!! " . $exception->getMessage() . "<br />";
		$i = 0;
		$traceLog = array_reverse($exception->getTrace());
		$traceResult = [];
		foreach ($traceLog as $trace)
		{
			if (
			    isset($trace['args'])
                && false === array_search('ErrorException', $trace['args'])
                && $trace['class'] != '\RenatUsTest\Throws\ErrorException'
                && $trace['function'] != '__call'
            ){
				array_push(
				    $traceResult,
                    ((isset($trace['file'])) ? ' file "' . $trace['file'] . '","' : '')
                    . ((isset($trace['line'])) ? ' line "' . $trace['line'] . '" :"' : '')
                    .((isset($trace['class'])) ? ' class - "' . $trace['class'] . '";"' : '')
                    .((isset($trace['function'])) ? ' method - "' . $trace['function'] . '";"' : '')
                    .(
                        (is_array($trace['args']) && !empty($trace['args']))
                            ? ' argumnets - \'' . implode("','", $trace['args']) .'\';'
                            : ''
                    )
				);
			}
			$i = $i + 1;
		}
		foreach ($traceResult as $key => $trace)
		{
			echo '#' . ($key + 1) . ' ' . $trace . '<br />';
		}
   }

   /**
    * Returns User Throw
    *
    * @return Throw
    */
   public static function showThrow($message)
   {
       ob_start();
       header("HTTP/1.0 404 Not Found");
       $target = array_reverse(debug_backtrace());
       echo '<br />';
       echo 'Warning!!! ' . $message . '<br />';
       $i = 0;
       $traceResult=array();
       foreach ($target as $trace){
           if(
               isset($trace['args'])
               && false===array_search('Dadiweb_Throw_ErrorException', $trace['args'])
               && $trace['function'] != '__call'
           ){
               array_push(
                   $traceResult,
                   ((isset($trace['file'])) ? ' file "' . $trace['file'] . '","' : '')
                   .((isset($trace['line'])) ? ' line "' . $trace['line'] . '" :"' : '')
                   .((isset($trace['class'])) ? ' class - "' . $trace['class'] . '";"' : '')
                   .((isset($trace['function'])) ? ' method - "' . $trace['function'] . '";"' : '')
                   .(
                       (is_array($trace['args']) && !empty($trace['args']))
                           ? ' argumnets - \'' . implode("','", $trace['args']) . '\';'
                           :''
                   )
			);
		}
		$i = $i + 1;
	}
	foreach ($traceResult as $key=>$trace){
		echo '#' . ($key + 1) . ' ' . $trace . '<br />';
	}
	ob_end_flush();
	exit;
   }
}
