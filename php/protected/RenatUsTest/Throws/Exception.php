<?php

namespace RenatUsTest\Throws;

/**
 * Class Exception
 * @package RenatUsTest\Throws
 * @author Denis Gubenko
 */
class Exception
{

    /**
     * Singleton instance
     *
     * @var null|\RenatUsTest\Throws\Exception
     */
    protected static $_instance = null;

	/**
     * Singleton pattern implementation makes "new" unavailable
     *
     * @return void
     */
	protected function __construct(){

	    return $this;
    }

	/**
     * Singleton pattern implementation makes "clone" unavailable
     *
     * @return void
     */
    protected function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Throws\Exception
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Throws\Exception Provides a fluent interface
     */
    public static function getInstance()
    {
    	if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * @param null $message
     * @param array $errorStack
     */
    public function getMessage($message = null, array $errorStack = [])
    {
    	http_response_code(404);
    	if(is_null($message) || empty($errorStack) && is_array($errorStack)){
	    	echo "Warning!!! Failed to initialize the error handler".'<br />';
    		$i = 1;
			$traceLog = array_reverse(debug_backtrace());
			$traceResult = [];
			foreach ($traceLog as $trace){
				if(
				    false === array_search('\RenatUsTest\Throws\Exception', $trace['args'])
                    && $trace['class'] != '\RenatUsTest\Throws\Exception'
                    && $trace['function'] != '__call'
                ){
					array_push(
					    $traceResult,
                        ((isset($trace['file'])) ? ' file "' . $trace['file'] . '","' : '')
                        . ((isset($trace['line'])) ? ' line "' . $trace['line'] . '" :"' : '')
                        . ((isset($trace['class'])) ? ' class - "' . $trace['class'] . '";"' : '')
                        . ((isset($trace['function'])) ? ' method - "' . $trace['function'] . '";"' : '')
                        . (
                            (!empty($trace['args']) && is_array($trace['args']))
                                ? ' argumnets - \'' . implode("','", $trace['args']) . '\';' : ''
                        )
					);
				}
				$i = $i + 1;
			}
			foreach ($traceResult as $key => $trace)
			{
				echo '#' . ($key+1) . ' ' . $trace . '<br />';
			}
			exit;
    	}
    	echo "Warning!!! ".$message.'<br />';
    	foreach ($errorStack as $key => $trace){
			echo '#' . ($key+1) . ' ' . $trace . '<br />';
		}
    }
}
