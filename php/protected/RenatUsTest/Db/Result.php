<?php

namespace RenatUsTest\Db;

use RenatUsTest\Throws\ErrorException;

/**
 * Class Result
 * @package RenatUsTest\Db
 * @author Denis Gubenko
 */
class Result
{
    /**
     * Handler variables that do not exist (input)
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    /**
     * Handler variables that do not exist (output)
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if (isset($this->{$name})) {
            return $this->{$name};
        }
        return null;
    }
    
    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorException::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}