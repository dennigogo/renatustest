<?php

namespace RenatUsTest\Db\Functions;

use RenatUsTest\Db\Config\Mongo as MongoConfig;
use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Helpers\Strings;
use RenatUsTest\Throws\ErrorExceptions;

use MongoDB\Client;
use MongoDB\Model\BSONDocument;
use MongoDB\InsertOneResult;
use MongoDB\InsertManyResult;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Query;
use MongoDB\Driver\ReadPreference;

/**
 * Class Mongo
 * @package RenatUsTest\Db\Functions
 * @author Denis Gubenko
 */
class Mongo extends AbstractFunctions
{
    /**
     * @var \RenatUsTest\Db\Config\Mongo|null
     */
    private $db = null;

    /**
     * Namespace of database
     * @var string|null
     */
    private $namespace = null;

    /**
     * @var string|null
     */
    private $dbName = null;

    /**
     * Mongo constructor.
     * @param \RenatUsTest\Db\Config\Mongo $options
     * @param string|null $namespace
     */
    public function __construct(MongoConfig $options, string $namespace = null)
    {
        $options->checkNamespace($namespace);
        $this->setNamespace($namespace);
        $options->checkConnect();
        $this->db = $options;
        $this->checkDbName();

    }

    /**
     * Set namespace of database
     * @param string|null $namespace
     */
    private function setNamespace(string $namespace = null)
    {
        $this->namespace = $namespace;
    }

    /**
     * Get namespace of database
     * @return string
     */
    public function getNamespace() :string
    {

        return $this->namespace;
    }

    /**
     * @return \RenatUsTest\Db\Config\Mongo
     */
    public function getConfig() :MongoConfig
    {
        return $this->db;
    }

    /**
     * Get connection of database
     * @return \MongoDb\Client
     */
    public function getConnect() :Client
    {
        if (false === $this->db instanceof MongoConfig) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Connection of database was not transmitted into class "%s"',
                    get_class($this)
                )
            );
        }

        return $this->db->getConnect();
    }

    private function checkDbName() :void
    {
        try {
            if (!empty(
            Arrays::getInstance()->arr2obj(
                $this->getConfig()->returnConfig()
            )->dbname
            )) {
                $this->setDbName(
                    $this->dbName = Arrays::getInstance()->arr2obj(
                        $this->getConfig()->returnConfig()
                    )->dbname
                );
            }
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" into class "%s" database name not set.',
                    __METHOD__,
                    get_class($this)
                )
            );
        }
    }

    private function setDbName(string $dbname) :void
    {
        $this->dbName = $dbname;
    }

    /**
     * @return string
     */
    private function getDbName() :string
    {
        return $this->dbName;
    }

    /**
     * @param string $dbName
     * @param string $collection
     * @return bool
     */
    public function drop(string $collection, string $dbName = null) :bool
    {
        $returned = false;
        if (false === Strings::getInstance()->emptyValidator($dbName)) {
            $this->setDbName($dbName);
        }
        try {
            if (false === Strings::getInstance()->emptyValidator($collection)) {
                $returned = $this->getConnect()->{$this->getDbName()}->{$collection}->drop();
                $returned = (bool) ($returned instanceof BSONDocument);
            }
        } catch (\Throwable $e) {

        }

        return $returned;
    }

    /**
     * @param string $collection
     * @param \stdClass $input
     * @param string|null $dbName
     * @return array|bool|InsertOneResult
     */
    public function insertOne(string $collection, \stdClass $input, string $dbName = null)
    {
        $returned = false;
        if (false === Strings::getInstance()->emptyValidator($dbName)) {
            $this->setDbName($dbName);
        }
        try {
            if (false === Strings::getInstance()->emptyValidator($collection) && !empty($input)) {
                $returned = $this->getConnect()->{$this->getDbName()}->{$collection}->insertOne($input);
                if (false !== $returned instanceof InsertOneResult) {
                    $returned = [ $returned->getInsertedCount(), $returned->getInsertedId() ];
                }
            }
        } catch (\Throwable $e) {

        }

        return $returned;
    }

    /**
     * @param string $collection
     * @param array $input
     * @param string|null $dbName
     * @return array|bool|InsertManyResult
     */
    public function insertMany(string $collection, array $input, string $dbName = null)
    {
        $returned = false;
        if (false === Strings::getInstance()->emptyValidator($dbName)) {
            $this->setDbName($dbName);
        }
        try {
            if (false === Strings::getInstance()->emptyValidator($collection) && !empty($input)) {
                $returned = $this->getConnect()->{$this->getDbName()}->{$collection}->insertMany($input);
                if (false !== $returned instanceof InsertManyResult) {
                    $returned = [ $returned->getInsertedCount(), $returned->getInsertedIds() ];
                }
            }
        } catch (\Throwable $e) {

        }

        return $returned;
    }

    /**
     * Execute a MongoDB query (full qualified namespace (databaseName.collectionName))
     * @link http://php.net/manual/en/mongodb-driver-manager.executequery.php
     * @param string $collection A part qualified namespace (collectionName)
     * @param Query $query A \MongoDB\Driver\Query to execute.
     * @param ReadPreference $readPreference Optionally, a \MongoDB\Driver\ReadPreference to route the command to. If none given, defaults to the Read Preferences set by the MongoDB Connection URI.
     * @param string|null $dbName A part qualified namespace (databaseName)
     * @return \MongoDB\Driver\Cursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function executeQuery(
        string $collection, Query $query, ReadPreference $readPreference = null, string $dbName = null
    ) :Cursor
    {
        $returned = null;
        if (false === Strings::getInstance()->emptyValidator($dbName)) {
            $this->setDbName($dbName);
        }
        try {
            $returned = $this->getConnect()->getManager()->executeQuery(
                $this->getDbName() . '.' . $collection, $query, $readPreference
            );
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error of database into class "%s". '
                    . ' "%s" ',
                    get_class($this),
                    $e->getMessage()
                )
            );
        }

        return $returned;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}