<?php

namespace RenatUsTest\Db\Functions;

use RenatUsTest\Db\Config\Pdo as PdoConfig;
use RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject as RenatUsTestSelect;
use RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject as RenatUsTestInsert;
use RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject as RenatUsTestUpdate;
use RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject as RenatUsTestDelete;
use RenatUsTest\Db\Functions\Common\Pdo\Random\RandomObject as RenatUsTestRandom;
use RenatUsTest\Db\Functions\Common\Pdo\Platform;
use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Helpers\Strings;
use RenatUsTest\Throws\ErrorExceptions;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Hydrator\Reflection as ReflectionHydrator;

/**
 * Class Pdo
 * @package RenatUsTest\Functions
 * @author Denis Gubenko
 */
class Pdo extends AbstractFunctions
{
    /**
     * @var PdoConfig|null
     */
    private $db = null;

    /**
     * Namespace of database
     * @var string|null
     */
    private $namespace = null;

    /**
     * Name table of database
     * @var string|null
     */
    private $table = null;

    /**
     * @var string
     */
    private $dirModels = "\\RenatUsTest\\Db\\Models\\Pdo\\Entities\\";

    /**
     * Pdo constructor.
     * @param PdoConfig $options
     * @param string|null $namespace
     */
    public function __construct(PdoConfig $options, string $namespace = null)
    {
        $options->checkNamespace($namespace);
        $this->setNamespace($namespace);
        $options->checkConnect();
        $this->db = $options;
    }

    /**
     * Set namespace of database
     * @param string|null $namespace
     */
    private function setNamespace(string $namespace = null)
    {
        $this->namespace = $namespace;
    }

    /**
     * Get namespace of database
     * @return string
     */
    public function getNamespace() :string
    {

        return $this->namespace;
    }

    /**
     * Get connection of database
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getConnect() :Adapter
    {
        if (false === $this->db instanceof PdoConfig) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Connection of database was not transmitted into class "%s"',
                    get_class($this)
                )
            );
        }

        return $this->db->getConnect();
    }

    /**
     * @return \RenatUsTest\Db\Config\Pdo
     */
    public function getConfig() :PdoConfig
    {
        return $this->db;
    }

    /**
     * Set table of database
     * @param string|null $table
     */
    private function setTable(string $table = null) : void
    {
        self::checkTable($table);
        $this->table = $table;
    }

    /**
     * Get table of database
     * @return string
     */
    public function getTable() :string
    {
        self::checkTable($this->table);

        return $this->table;
    }

    /**
     * @param string|null $table
     * @return bool
     */
    private function checkTable(string $table = null) :bool
    {
        if (false !== Strings::getInstance()->emptyValidator($table)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Table of database was not transmitted into class "%s"',
                    get_class($this)
                )
            );
        }

        return true;
    }

    /**
     * @return string
     */
    private function getDirModels()
    {

        return $this->dirModels;
    }

    /**
     * Quotation of identifier
     *
     * @param string|null $input
     * @return string
     */
    private function quoteIdentifier(string $input = null) :string
    {
        if (false !== Strings::getInstance()->emptyValidator($input)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Quotation of identifier column error occurred. '
                    . 'Parameter `input:string` was not transmitted into method "%s"',
                    __METHOD__
                )
            );
        }
        $returned = $this->getConnect()->platform->quoteIdentifier($input);

        return $returned;
    }

    /**
     * Quotation of value
     *
     * @param string|null $input
     * @return string
     */
    private function quoteValue(string $input = null) :string
    {
        if (false !== Strings::getInstance()->emptyValidator($input)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Quotation of columns value error occurred. '
                    . 'Parameter `input:string` was not transmitted into method "%s"',
                    __METHOD__
                )
            );
        }
        $returned = $this->getConnect()->platform->quoteValue($input);

        return $returned;
    }

    /**
     * @see https://docs.zendframework.com/zend-db/sql/#select
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject $input
     * @return array
     * @throws \ReflectionException
     */
    public function select(RenatUsTestSelect $input) :array
    {
        $returned = [];
        $createStatement = $this->db->getConnect()->createStatement(
            $this->db->select($this->getTable(), $input)
                ->getSqlString(Platform::getInstance()->getAdapterPlatform())
            . $this->checkLimit($input) . $this->checkOffset($input)
        );
        $createStatement->prepare();
        $result = $createStatement->execute();
        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            $class = $this->getDirModels() . ucfirst($this->getTable());
            $hydratingResult = new HydratingResultSet(new ReflectionHydrator, new $class);
            $hydratingResult->initialize($result);
            foreach ($hydratingResult as $item) {
                $reflection = new \ReflectionClass($item);
                $preg = "/^get[A-Z]{1}[a-z0-9_]*$/";
                $array = [];
                $methods = $reflection->getMethods(\ReflectionMethod::IS_PUBLIC);
                if (!empty($methods) && is_array($methods)) {
                    foreach ($methods as $method) {
                        if (false !== (bool) preg_match_all($preg, $method->getName())) {
                            $keys = mb_split('get', $method->getName());
                            if (in_array(lcfirst($keys[1]), array_flip(
                                        array_change_key_case(
                                            array_flip(
                                                $input->getColumns()[0]
                                            ),
                                            CASE_LOWER
                                        )))
                            ) {
                                $array[lcfirst($keys[1])] = $item->{$method->getName()}();
                            }
                        }
                    }
                }
                array_push($returned, Arrays::getInstance()->arr2obj($array));
            }
        }

        return $returned;
    }

    /**
     * @see https://docs.zendframework.com/zend-db/sql/#insert
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject $input
     * @return bool|int
     */
    public function insert(RenatUsTestInsert $input)
    {
        $createStatement = $this->db->getConnect()->createStatement(
            $this->db->insert($this->getTable(), $input)
                ->getSqlString(Platform::getInstance()->getAdapterPlatform())
        );
        $createStatement->prepare();
        $createStatement->execute();
        $result = $this->db->getConnect()->getDriver()->getLastGeneratedValue();
        $returned = (false !== (bool) $result) ? $result : false;

        return $returned;
    }

    /**
     * @see https://docs.zendframework.com/zend-db/sql/#update
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject $input
     * @return bool|int
     */
    public function update(RenatUsTestUpdate $input)
    {
        $createStatement = $this->db->getConnect()->createStatement(
            $this->db->update($this->getTable(), $input)
                ->getSqlString(Platform::getInstance()->getAdapterPlatform())
        );
        $createStatement->prepare();
        $createStatement->execute();

        return true;
    }

    /**
     * @see https://docs.zendframework.com/zend-db/sql/#delete
     * @param RenatUsTestDelete $input
     * @return bool
     */
    public function delete(RenatUsTestDelete $input)
    {
        $createStatement = $this->db->getConnect()->createStatement(
            $this->db->delete($this->getTable(), $input)
                ->getSqlString(Platform::getInstance()->getAdapterPlatform())
        );
        $createStatement->prepare();
        $createStatement->execute();

        return true;
    }

    /**
     * @param RenatUsTestRandom $object
     * @return bool
     */
    public function randomQuery(RenatUsTestRandom $object) :bool
    {
        $returned = $this->db->randomQuery($object);

        return $returned;
    }

    /**
     * @param $name
     * @return $this
     */
    public function __get($name)
    {
        $name = (
            (empty($this->db->getPrefix()) ? '' : $this->db->getPrefix()) . $name
        );
        try {
            $this->db->getMetadata()->getTable($name);
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error into class "%s". Table %s was not created.',
                    get_class($this),
                    $name
                )
            );
        }
        self::setTable($name);

        return $this;
    }

    /**
     * @param RenatUsTestSelect $input
     * @return string
     */
    private function checkLimit(RenatUsTestSelect $input) :string
    {
        $returned = "";
        if (false === Strings::getInstance()->emptyValidator($input->getLimit())) {
            $returned = " LIMIT " . $input->getLimit() . " ";
        }

        return $returned;
    }

    /**
     * @param RenatUsTestSelect $input
     * @return string
     */
    private function checkOffset(RenatUsTestSelect $input) :string
    {
        $returned = "";
        if (false === Strings::getInstance()->emptyValidator($input->getOffset())) {
            $returned = " OFFSET " . $input->getOffset() . " ";
        }

        return $returned;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}