<?php

namespace RenatUsTest\Db\Functions\Common\Pdo\Random;

use RenatUsTest\Throws\ErrorExceptions;

/**
 * Class RandomObject
 * @package RenatUsTest\Db\Functions\Common\Pdo\Random
 * @author Denis Gubenko
 */
class RandomObject
{

    /**
     * @var array
     */
    private $items = [];

    /**
     * @var string|null
     */
    private $sql = null;

    /**
     * Arrays constructor.
     */
    public function __construct(){
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSql(): ?string
    {
        return $this->sql;
    }

    /**
     * @param string $sql
     */
    public function setSql(string $sql): void
    {
        $this->sql = $sql;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}