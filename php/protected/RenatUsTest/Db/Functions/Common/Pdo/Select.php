<?php

namespace RenatUsTest\Db\Functions\Common\Pdo;

use RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject as RenatUsTestSelect;
use RenatUsTest\Throws\ErrorExceptions;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select as ZendSelect;
use Zend\Db\Adapter\AdapterInterface;

/**
 * Class Select
 * @package RenatUsTest\Db\Functions\Common\Pdo
 * @author Denis Gubenko
 */
class Select
{
    /**
     * @var \Zend\Db\Sql\Sql|null
     */
    private $sqlObject = null;

    /**
     * @var \RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject|null
     */
    private $selectObject = null;

    /**
     * @var \Zend\Db\Sql\Select|null
     */
    private $parentObject = null;

    /**
     * @var string|null
     */
    private $table = null;

    /**
     * Select constructor.
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     * @param string $table
     * @param RenatUsTestSelect $object
     */
    public function __construct(AdapterInterface $adapter, string $table, RenatUsTestSelect $object)
    {
        $this->setSqlObject(new Sql($adapter, null, null));
        $this->setSelectObject($object);
        $this->setTable($table);
        $this->setParentObject();
        $this->router();
    }

    private function router() :void
    {
        if (false === $this->selectObject instanceof RenatUsTestSelect) {
            ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "selectObject" '
                    . 'not instance with class "\RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject" '
                    . 'with class "%s"',
                    get_class($this)
                )
            );
        }
        $this->checkWhere();
        $this->checkColumns();
        $this->checkJoin();
        $this->checkGroup();
        $this->checkHaving();
        $this->checkOrder();
    }

    /**
     * @return \Zend\Db\Sql\Sql
     */
    private function getSqlObject() :Sql
    {

        return $this->sqlObject;
    }

    /**
     * @param \Zend\Db\Sql\Sql $object
     */
    private function setSqlObject(Sql $object)
    {
        $this->sqlObject = $object;
    }

    /**
     * @return null|string
     */
    private function getTable()
    {
        return $this->table;
    }

    /**
     * @param string|null $table
     */
    private function setTable(string $table = null)
    {
        if (!empty($table) && is_string($table)) {
            $this->table = $table;
        }
    }

    /**
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject
     */
    private function getSelectObject() :RenatUsTestSelect
    {

        return $this->selectObject;
    }

    /**
     * @param RenatUsTestSelect $object
     */
    private function setSelectObject(RenatUsTestSelect $object)
    {
        $this->selectObject = $object;
    }

    /**
     * @return \Zend\Db\Sql\Select
     */
    public function getParentObject() :ZendSelect
    {

        return $this->parentObject;
    }

    /**
     * @param \Zend\Db\Sql\Select|null $object
     */
    private function setParentObject(ZendSelect $object = null) :void
    {
        if (false !== $object instanceof ZendSelect) {
            $this->parentObject = $object;
        } else {
            $this->parentObject = $this->getSqlObject()->select($this->getTable());
        }
    }

    private function checkWhere() :void
    {
        $where = $this->getSelectObject()->getWhere();
        if (!empty($where) && is_array($where)) {
            $this->setParentObject($this->getParentObject()->where($where[0], $where[1]));
        }
    }

    private function checkColumns() :void
    {
        $columns = $this->getSelectObject()->getColumns();
        if (!empty($columns) && is_array($columns)) {
            $this->setParentObject($this->getParentObject()->columns($columns[0], $columns[1]));
        }
    }

    private function checkJoin() :void
    {
        $join = $this->getSelectObject()->getJoin();
        if (!empty($join) && is_array($join)) {
            $this->setParentObject($this->getParentObject()->join($join[0], $join[1], $join[2], $join[3]));
        }
    }

    private function checkGroup() :void
    {
        $group = $this->getSelectObject()->getGroup();
        if (!empty($group) && is_array($group)) {
            $this->setParentObject($this->getParentObject()->group($group));
        }
    }

    private function checkHaving() :void
    {
        $having = $this->getSelectObject()->getHaving();
        if (!empty($having) && is_array($having)) {
            $this->setParentObject($this->getParentObject()->having($having[0], $having[1]));
        }
    }

    private function checkOrder() :void
    {
        $order = $this->getSelectObject()->getOrder();
        if (!empty($order) && (is_string($order) || is_array($order))) {
            $this->setParentObject($this->getParentObject()->order($order));
        }
    }
}