<?php

namespace RenatUsTest\Db\Functions\Common\Pdo\Select;

use RenatUsTest\Helpers\Strings;
use RenatUsTest\Throws\ErrorException;
use Zend\Db\Sql\Select as ZendSelect;

/**
 * Class Object
 * @package RenatUsTest\Db\Functions\Common\Pdo\SelectObject
 * @author Denis Gubenko
 */
class SelectObject
{
    /**
     * @var array
     */
    private $columns = [];

    /**
     * @var array
     */
    private $where = [];

    /**
     * @var array
     */
    private $join = [];

    /**
     * @var array
     */
    private $group = [];

    /**
     * @var array
     */
    private $having = [];

    /**
     * @var string|array
     */
    private $order = null;

    /**
     * @var string|null
     */
    private $limit = null;

    /**
     * @var string|null
     */
    private $offset = null;

    /**
     * Arrays constructor.
     */
	public function __construct(){
	    return $this;
    }

    /**
     * @return array
     */
    public function getWhere()
    {
        return $this->where;
    }

    /**
     * @param  \Zend\Db\Sql\Where|\Closure|string|array|\Zend\Db\Sql\Predicate\PredicateInterface $predicate
     * @param  string $combination One of the OP_* constants from \Zend\Db\Sql\Predicate\PredicateSet
     * @return SelectObject
     */
    public function setWhere($predicate, $combination = \Zend\Db\Sql\Predicate\PredicateSet::OP_AND) :self
    {
        $this->where = [ $predicate,  $combination ];

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param  array $columns
     * @param  bool $prefixColumnsWithTable
     * @return SelectObject
     */
    public function setColumns(array $columns = [], $prefixColumnsWithTable = true): self
    {
        if (!empty($columns) && is_array($columns)) {
            $this->columns = [ $columns, $prefixColumnsWithTable ];
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getJoin() :array
    {

        return $this->join;
    }

    /**
     * @param  string|array|\Zend\Db\Sql\TableIdentifier $name
     * @param  string|\Zend\Db\Sql\Predicate|\Zend\Db\Sql\Expression $on
     * @param  string|array $columns
     * @param  string $type one of the JOIN_* constants
     * @return $this
     */
    public function setJoin($name, $on, $columns = ZendSelect::SQL_STAR, $type = ZendSelect::JOIN_INNER)
    {
        $this->join = [ $name, $on, $columns, $type ];

        return $this;
    }

    /**
     * @return array
     */
    public function getGroup(): array
    {
        return $this->group;
    }

    /**
     * @param array $group
     * @return SelectObject
     */
    public function setGroup(array $group = []): self
    {
        if (!empty($group) && is_array($group)) {
            $this->group = $group;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getHaving(): array
    {
        return $this->having;
    }

    /**
     * Create having clause
     *
     * @param  \Zend\Db\Sql\Where|\Closure|string|array $predicate
     * @param  string $combination One of the OP_* constants from \Zend\Db\Sql\Predicate\PredicateSet
     * @return SelectObject
     */
    public function setHaving($predicate, $combination = \Zend\Db\Sql\Predicate\PredicateSet::OP_AND): self
    {
        $this->having = [ $predicate, $combination ];

        return $this;
    }

    /**
     * @return string|array
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string|array $order
     * @return SelectObject
     */
    public function setOrder($order): self
    {
        if (!empty($order) && (is_string($order) || is_array($order))) {
            $this->order = $order;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getLimit() :string
    {
        return (is_null($this->limit)) ? '' : $this->limit;
    }

    /**
     * @param string $limit
     * @return SelectObject
     */
    public function setLimit($limit): self
    {
        if (false === Strings::getInstance()->emptyValidator($limit)) {
            $this->limit = $limit;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getOffset(): string
    {
        return (is_null($this->offset)) ? "" : $this->offset;
    }

    /**
     * @param string $offset
     * @return SelectObject
     */
    public function setOffset($offset): self
    {
        if (false === Strings::getInstance()->emptyValidator($offset)) {
            $this->offset = $offset;
        }

        return $this;
    }

    /**
     * @param $method
     * @param $args
     */
	public function __call($method, $args)
    {
    	if (!method_exists($this, $method)) {
         	throw ErrorExceptions::getInstance()->getMessage(
         	    sprintf(
         	        'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
       	} 	
    }
}