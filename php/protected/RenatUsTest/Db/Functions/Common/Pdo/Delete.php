<?php

namespace RenatUsTest\Db\Functions\Common\Pdo;

use RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject as RenatUsTestDelete;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Delete as ZendDelete;
use Zend\Db\Adapter\AdapterInterface;

class Delete
{
    /**
     * @var \Zend\Db\Sql\Sql|null
     */
    private $sqlObject = null;

    /**
     * @var \RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject|null
     */
    private $deleteObject = null;

    /**
     * @var \Zend\Db\Sql\Delete|null
     */
    private $parentObject = null;

    /**
     * @var string|null
     */
    private $table = null;

    /**
     * Insert constructor.
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     * @param string $table
     * @param RenatUsTestDelete $object
     */
    public function __construct(AdapterInterface $adapter, string $table, RenatUsTestDelete $object)
    {
        $this->setSqlObject(new Sql($adapter, null, null));
        $this->setDeleteObject($object);
        $this->setTable($table);
        $this->setParentObject();
        $this->router();
    }

    private function router() :void
    {
        if (false === $this->deleteObject instanceof RenatUsTestDelete) {
            ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "deleteObject" '
                    . 'not instance with class "\RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject" '
                    . 'with class "%s"',
                    get_class($this)
                )
            );
        }
        $this->checkWhere();
    }

    /**
     * @return \Zend\Db\Sql\Sql
     */
    private function getSqlObject() :Sql
    {

        return $this->sqlObject;
    }

    /**
     * @param \Zend\Db\Sql\Sql $object
     */
    private function setSqlObject(Sql $object)
    {
        $this->sqlObject = $object;
    }

    /**
     * @return null|string
     */
    private function getTable()
    {
        return $this->table;
    }

    /**
     * @param string|null $table
     */
    private function setTable(string $table = null)
    {
        if (!empty($table) && is_string($table)) {
            $this->table = $table;
        }
    }

    /**
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject
     */
    private function getDeleteObject() :RenatUsTestDelete
    {

        return $this->deleteObject;
    }

    /**
     * @param RenatUsTestDelete $object
     */
    private function setDeleteObject(RenatUsTestDelete $object)
    {
        $this->deleteObject = $object;
    }

    /**
     * @return \Zend\Db\Sql\Delete
     */
    public function getParentObject() :ZendDelete
    {

        return $this->parentObject;
    }

    /**
     * @param \Zend\Db\Sql\Delete|null $object
     */
    private function setParentObject(ZendDelete $object = null) :void
    {
        if (!is_null($object) && false !== $object instanceof ZendDelete) {
            $this->parentObject = $object;
        } else {
            $this->parentObject = $this->getSqlObject()->delete($this->getTable());
        }
    }

    private function checkWhere() :void
    {
        $where = $this->getDeleteObject()->getWhere();
        if (!empty($where) && is_array($where)) {
            $this->setParentObject($this->getParentObject()->where($where[0], $where[1]));
        }
    }
}