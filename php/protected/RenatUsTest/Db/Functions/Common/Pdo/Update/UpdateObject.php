<?php

namespace RenatUsTest\Db\Functions\Common\Pdo\Update;

use RenatUsTest\Throws\ErrorExceptions;
use Zend\Db\Sql\Update as ZendUpdate;

/**
 * Class UpdateObject
 * @package RenatUsTest\Db\Functions\Common\Pdo\Update
 * @author Denis Gubenko
 */
class UpdateObject
{

    /**
     * @var array
     */
    private $set = [];

    /**
     * @var array
     */
    private $where = [];

    /**
     * Arrays constructor.
     */
    public function __construct(){
        return $this;
    }

    /**
     * @return array
     */
    public function getSet(): array
    {
        return $this->set;
    }

    /**
     * @param array $values Associative array of key values
     * @param string $flag One of the VALUES_* constants
     * @return UpdateObject
     */
    public function setSet(array $values, $flag = ZendUpdate::VALUES_SET): self
    {
        $this->set = [ $values, $flag ];

        return $this;
    }

    /**
     * @return array
     */
    public function getWhere() :array
    {
        return $this->where;
    }

    /**
     * @param \Zend\Db\Sql\Where|\Closure|string|array $predicate
     * @param string $combination One of the OP_* constants from Predicate\PredicateSet
     * @return UpdateObject
     */
    public function setWhere($predicate, $combination = \Zend\Db\Sql\Predicate\PredicateSet::OP_AND) :self
    {
        $this->where = [ $predicate, $combination ];

        return $this;
    }

    /**
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}