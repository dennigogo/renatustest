<?php

namespace RenatUsTest\Db\Functions\Common\Pdo\Delete;

use RenatUsTest\Throws\ErrorExceptions;

/**
 * Class DeleteObject
 * @package RenatUsTest\Db\Functions\Common\Pdo\Delete
 * @author Denis Gubenko
 */
class DeleteObject
{

    /**
     * @var array
     */
    private $where = [];

    /**
     * Arrays constructor.
     */
    public function __construct(){
        return $this;
    }

    /**
     * @return array
     */
    public function getWhere() :array
    {
        return $this->where;
    }

    /**
     * @param \Zend\Db\Sql\Where|\Closure|string|array $predicate
     * @param string $combination One of the OP_* constants from Predicate\PredicateSet
     * @return UpdateObject
     */
    public function setWhere($predicate, $combination = \Zend\Db\Sql\Predicate\PredicateSet::OP_AND) :self
    {
        $this->where = [ $predicate, $combination ];

        return $this;
    }

    /**
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}