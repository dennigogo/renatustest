<?php

namespace RenatUsTest\Db\Functions\Common\Pdo;

use RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject as RenatUsTestUpdate;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Update as ZendUpdate;
use Zend\Db\Adapter\AdapterInterface;

class Update
{
    /**
     * @var \Zend\Db\Sql\Sql|null
     */
    private $sqlObject = null;

    /**
     * @var \RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject|null
     */
    private $updateObject = null;

    /**
     * @var \Zend\Db\Sql\Update|null
     */
    private $parentObject = null;

    /**
     * @var string|null
     */
    private $table = null;

    /**
     * Insert constructor.
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     * @param string $table
     * @param RenatUsTestUpdate $object
     */
    public function __construct(AdapterInterface $adapter, string $table, RenatUsTestUpdate $object)
    {
        $this->setSqlObject(new Sql($adapter, null, null));
        $this->setUpdateObject($object);
        $this->setTable($table);
        $this->setParentObject();
        $this->router();
    }

    private function router() :void
    {
        if (false === $this->updateObject instanceof RenatUsTestUpdate) {
            ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "updateObject" '
                    . 'not instance with class "\RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject" '
                    . 'with class "%s"',
                    get_class($this)
                )
            );
        }
        $this->checkSet();
        $this->checkWhere();
    }

    /**
     * @return \Zend\Db\Sql\Sql
     */
    private function getSqlObject() :Sql
    {

        return $this->sqlObject;
    }

    /**
     * @param \Zend\Db\Sql\Sql $object
     */
    private function setSqlObject(Sql $object)
    {
        $this->sqlObject = $object;
    }

    /**
     * @return null|string
     */
    private function getTable()
    {
        return $this->table;
    }

    /**
     * @param string|null $table
     */
    private function setTable(string $table = null)
    {
        if (!empty($table) && is_string($table)) {
            $this->table = $table;
        }
    }

    /**
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject
     */
    private function getUpdateObject() :RenatUsTestUpdate
    {

        return $this->updateObject;
    }

    /**
     * @param RenatUsTestUpdate $object
     */
    private function setUpdateObject(RenatUsTestUpdate $object)
    {
        $this->updateObject = $object;
    }

    /**
     * @return \Zend\Db\Sql\Update
     */
    public function getParentObject() :ZendUpdate
    {

        return $this->parentObject;
    }

    /**
     * @param \Zend\Db\Sql\Update|null $object
     */
    private function setParentObject(ZendUpdate $object = null) :void
    {
        if (!is_null($object) && false !== $object instanceof ZendUpdate) {
            $this->parentObject = $object;
        } else {
            $this->parentObject = $this->getSqlObject()->update($this->getTable());
        }
    }

    private function checkSet() :void
    {
        $set = $this->getUpdateObject()->getSet();
        if (!empty($set) && is_array($set)) {
            $this->setParentObject($this->getParentObject()->set($set[0], $set[1]));
        }
    }

    private function checkWhere() :void
    {
        $where = $this->getUpdateObject()->getWhere();
        if (!empty($where) && is_array($where)) {
            $this->setParentObject($this->getParentObject()->where($where[0], $where[1]));
        }
    }
}