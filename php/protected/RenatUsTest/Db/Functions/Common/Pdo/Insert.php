<?php

namespace RenatUsTest\Db\Functions\Common\Pdo;

use RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject as RenatUsTestInsert;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Insert as ZendInsert;
use Zend\Db\Adapter\AdapterInterface;

class Insert
{
    /**
     * @var \Zend\Db\Sql\Sql|null
     */
    private $sqlObject = null;

    /**
     * @var \RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject|null
     */
    private $insertObject = null;

    /**
     * @var \Zend\Db\Sql\Insert|null
     */
    private $parentObject = null;

    /**
     * @var string|null
     */
    private $table = null;

    /**
     * Select constructor.
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     * @param null $table
     * @param \Zend\Db\Sql\Platform\AbstractPlatform|null $sqlPlatform
     */

    /**
     * Insert constructor.
     * @param \Zend\Db\Adapter\AdapterInterface $adapter
     * @param string $table
     * @param RenatUsTestInsert $object
     */
    public function __construct(AdapterInterface $adapter, string $table, RenatUsTestInsert $object)
    {
        $this->setSqlObject(new Sql($adapter, null, null));
        $this->setInsertObject($object);
        $this->setTable($table);
        $this->setParentObject();
        $this->router();
    }

    private function router() :void
    {
        if (false === $this->insertObject instanceof RenatUsTestInsert) {
            ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "insertObject" '
                    . 'not instance with class "\RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject" '
                    . 'with class "%s"',
                    get_class($this)
                )
            );
        }
        $this->checkColumns();
        $this->checkValues();
    }

    /**
     * @return \Zend\Db\Sql\Sql
     */
    private function getSqlObject() :Sql
    {

        return $this->sqlObject;
    }

    /**
     * @param \Zend\Db\Sql\Sql $object
     */
    private function setSqlObject(Sql $object)
    {
        $this->sqlObject = $object;
    }

    /**
     * @return null|string
     */
    private function getTable()
    {
        return $this->table;
    }

    /**
     * @param string|null $table
     */
    private function setTable(string $table = null)
    {
        if (!empty($table) && is_string($table)) {
            $this->table = $table;
        }
    }

    /**
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject
     */
    private function getInsertObject() :RenatUsTestInsert
    {

        return $this->insertObject;
    }

    /**
     * @param RenatUsTestInsert $object
     */
    private function setInsertObject(RenatUsTestInsert $object)
    {
        $this->insertObject = $object;
    }

    /**
     * @return \Zend\Db\Sql\Insert
     */
    public function getParentObject() :ZendInsert
    {

        return $this->parentObject;
    }

    /**
     * @param \Zend\Db\Sql\Insert|null $object
     */
    private function setParentObject(ZendInsert $object = null) :void
    {
        if (!is_null($object) && false !== $object instanceof ZendInsert) {
            $this->parentObject = $object;
        } else {
            $this->parentObject = $this->getSqlObject()->insert($this->getTable());
        }
    }

    private function checkColumns() :void
    {
        $columns = $this->getInsertObject()->getColumns();
        if (!empty($columns) && is_array($columns)) {
            $this->setParentObject($this->getParentObject()->columns($columns));
        }
    }

    private function checkValues() :void
    {
        $values = $this->getInsertObject()->getValues();
        if (!empty($values) && is_array($values)) {
            $this->setParentObject($this->getParentObject()->values($values[0], $values[1]));
        }
    }
}