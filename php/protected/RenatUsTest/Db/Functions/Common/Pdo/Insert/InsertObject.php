<?php

namespace RenatUsTest\Db\Functions\Common\Pdo\Insert;

use Zend\Db\Sql\Insert as ZendInsert;

use RenatUsTest\Throws\ErrorExceptions;

/**
 * Class InsertObject
 * @package RenatUsTest\Db\Functions\Common\Pdo\Insert
 * @author Denis Gubenko
 */
class InsertObject
{

    /**
     * @var array
     */
    private $columns = [];

    /**
     * @var array
     */
    private $values = [];

    /**
     * Arrays constructor.
     */
    public function __construct(){
        return $this;
    }

    /**
     * @return array
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

    /**
     * @param  array $columns
     * @return InsertObject
     */
    public function setColumns(array $columns = []): self
    {
        if (!empty($columns) && is_array($columns)) {
            $this->columns = $columns;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @param array $values
     * @param string $flag
     * @return InsertObject
     */
    public function setValues(array $values, $flag = ZendInsert::VALUES_SET): self
    {
        $this->values = [ $values, $flag ];

        return $this;
    }

    /**
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}