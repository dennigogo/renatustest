<?php

namespace RenatUsTest\Db\Functions\Common\Pdo;

use RenatUsTest\Db\Config\Pdo as PdoConfig;
use RenatUsTest\Throws\ErrorExceptions;

use Zend\Db\Adapter\Platform\AbstractPlatform;
use Zend\Db\Adapter\Platform\Mysql as ZendMysql;
use Zend\Db\Adapter\Platform\Postgresql as ZendPostgresql;
use Zend\Db\Adapter\Platform\Oracle as ZendOracle;
use Zend\Db\Adapter\Platform\IbmDb2 as ZendIbmDb2;
use Zend\Db\Adapter\Platform\Sql92 as ZendSql92;
use Zend\Db\Adapter\Platform\Sqlite as ZendSqlite;
use Zend\Db\Adapter\Platform\SqlServer as ZendSqlServer;

/**
 * Class Platform
 * @package RenatUsTest\Db\Functions\Common\Pdo
 * @author DenisGubenko
 */
class Platform
{

    /**
     * Singleton instance
     * @var \RenatUsTest\Db\Functions\Common\Pdo\Platform|null
     */
    private static $instance = null;

    private $adapterPlatform = null;

    /**
     * Db constructor.
     */
    private function __construct(){

        return $this;
    }

    /**
     * Singleton pattern implementation makes "clone" unavailable
     * @return void
     */
    private function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Db\Functions\Common\Pdo\Platform
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Platform
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Reset instance of \RenatUsTest\Db\Functions\Common\Pdo\Platform
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Db\Functions\Common\Pdo\Platform
     */
    public static function resetInstance()
    {
        return self::$instance = null;
    }

    /**
     * @param PdoConfig $config
     * @return Platform
     */
    public function setupPlatform(PdoConfig $config) :self
    {
        switch (true) {
            case ($config->getPlatform() === 'ZendMysql') :
                $this->setAdapterPlatform(new ZendMysql($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendPostgresql') :
                $this->setAdapterPlatform(new ZendPostgresql($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendOracle') :
                $this->setAdapterPlatform(new ZendOracle($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendIbmDb2') :
                $this->setAdapterPlatform(new ZendIbmDb2($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendSql92') :
                $this->setAdapterPlatform(new ZendSql92($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendSqlite') :
                $this->setAdapterPlatform(new ZendSqlite($config->getConnect()->getDriver()));
                break;
            case ($config->getPlatform() === 'ZendSqlServer') :
                $this->setAdapterPlatform(new ZendSqlServer($config->getConnect()->getDriver()));
                break;
            default:
                throw ErrorExceptions::showThrow(
                    sprintf(
                        'Critical error. Variable "db.pdo.%s.platform" '
                        . 'from the file "%sdb.ini" was not transmitted in class "%s". '
                        . 'Expected values list: ' . print_r([
                            'Zend\Db\Adapter\Platform\Mysql',
                            'Zend\Db\Adapter\Platform\Postgresql',
                            'Zend\Db\Adapter\Platform\Oracle',
                            'Zend\Db\Adapter\Platform\IbmDb2',
                            'Zend\Db\Adapter\Platform\Sql92',
                            'Zend\Db\Adapter\Platform\Sqlite',
                            'Zend\Db\Adapter\Platform\SqlServer'
                        ], true),
                        $config->getNamespace(),
                        INI_PATH,
                        get_class($this)
                    )
                );
                break;
        }

        return $this;
    }

    /**
     * @param AbstractPlatform $input
     */
    private function setAdapterPlatform(AbstractPlatform $input) {
        $this->adapterPlatform = $input;
    }

    /**
     * @return AbstractPlatform
     */
    public function getAdapterPlatform() :AbstractPlatform
    {
        return $this->adapterPlatform;
    }

    /**
     * The handler functions that do not exist
     * @return void
     */
    public function __call($method, $args)
    {
        if(!method_exists($this, $method)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}