<?php

namespace RenatUsTest\Db;

use RenatUsTest\Throws\ErrorException;

/**
 * Class Config
 * @package RenatUsTest\Db
 * @author Denis Gubenko
 */
class Config
{
    /**
     * Config of database
     * 
     * @var array
     */
    protected $config = null;
    
    /**
     * Connect into database
     *
     * @var PDO
     */
    protected $connect = null;

    /**
     * Init PDO driver
     * 
     * @param array $options
     */
    public function __construct(array $options, $namespace = null)
    {
        if (is_null($namespace)) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Namespace of database was not transmitted in class "%s"',
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['driver'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.driver" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,get_class($this)
                )
            );
        }

        if (!isset($options['dbname'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.dbname" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['username'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.username" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,get_class($this))
            );
        }

        if (!isset($options['password'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.password" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['host'])) {
            throw ErrorException::showThrow(
                sprintf('Critical error. Variable "db.pdo.%s.host" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['prefix'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.prefix" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['collation'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.collation" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        if (!isset($options['charset'])) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.charset" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }

        $this->setConfig($options);
        $this->setConnect();
    }

    /**
     * Set config for database
     *
     * @param array
     */
    protected function setConfig(array $config = []){
        if(!count($config)){
            throw ErrorException::showThrow(
                    sprintf(
                        'Critical error. '
                        . 'Config of database was not transmitted into method "%s"',
                        __METHOD__
                    )
            );
        }

        $this->config = Dadiweb_Aides_Array::getInstance()->arr2obj($config);
    }
    
    /**
     * Get config for database
     *
     * @return array
     */
    public function getConfig()
    {
        if (empty($this->config)) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. '
                    . 'Config of database was not transmitted into method "%s"',
                    __METHOD__
                )
            );
        }

        return $this->config;
    }

    /**
     * Set connection into database
     *
     * @param nothing
     */
    protected function setConnect()
    {
        $config = $this->getConfig();
        if (empty($config)) {
            throw ErrorException::showThrow(
                sprintf(
                    'Critical error. '
                    . 'Config of database was not transmitted into method "%s"',
                        __METHOD__
                    )
            );
        }
        
        try {
            $this->connect = new PDO(
                    $config->driver . ':dbname=' . $config->dbname . ';host=' . $config->host,
                    $config->username,
                    $config->password,
                    [
                        PDO::ATTR_PERSISTENT => true, //establish a permanent connection
                    ]
            );
        } catch (\PDOException $e) {
            throw ErrorException::showThrow(
                    sprintf('Critical error into method "%s" (%s)', __METHOD__,$e->getMessage())
            );
        }
    }

    /**
     * Get connection for database
     *
     * @return array
     */
    public function getConnect()
    {
        if (is_null($this->connect)) {
            throw ErrorException::showThrow(
                    sprintf(
                        'Critical error. '
                        . 'Into method "%s" connection with the database is not set',
                        __METHOD__
                    )
            );
        }

        return $this->connect;
    }

    /**
     *
     * The handler functions that do not exist
     *
     * @return Exeptions, default - NULL
     *
     */
    public function __call($method, $args)
    {
        if(!method_exists($this, $method)) {
            throw ErrorException::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}