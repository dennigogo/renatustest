<?php

namespace RenatUsTest\Db\Table;

use RenatUsTest\Config\Kernel;
use RenatUsTest\Db\Table\AbstractClass;
use RenatUsTest\Helpers\Arrays;

/**
 * Class TableClass
 * @package RenatUsTest\Db\Table
 * @author Denis Gubenko
 */
class TableClass extends AbstractClass
{
    /**
     * Default set variables
     *
     * @var Object
     */
    
    protected $variables = array();
    
    /**
     * DB class of Dadiweb_Configuration_Db_Functions
     *
     * @var array
     */
    protected $db = NULL;
    
/***************************************************************/
    /**
     * Init method
     * 
     * @see Dadiweb_Db_Table_Abstract::initDb()
     */
    public function initDb(){

    }
    
/***************************************************************/
    /**
     *
     * Handler variables that do not exist (input)
     *
     * @return Nothing
     *
     */
    public function __set($name, $value)
    {
        $this->variables[$name] = $value;
    }
/***************************************************************/
    /**
     *
     * Handler variables that do not exist (output)
     *
     * @return $this->variables
     *
     */
    public function __get($name)
    {
        if (array_key_exists($name, Kernel::getInstance()->getDb())) {
            return $this->db = Arrays::getInstance()->arr2obj(
                Kernel::getInstance()->getDb()
            )->{$name};
        }
        if (array_key_exists($name, $this->variables)) {
            return $this->variables[$name];
        }

        return null;
    }
    
    /**
     *
     * The handler functions that do not exist
     *
     * @return Exeption, default - NULL
     *
     */
    public function __call($method, $args)
    {
        if(!method_exists($this, $method)) {
            throw ErrorException::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}