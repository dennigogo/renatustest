<?php

namespace RenatUsTest\Db\Tables\Pdo\Entities;

/**
 * Class Sss
 * @package RenatUsTest\Db\Tables\Pdo\Entities
 * @author Denis Gubenko
 */
class Sss
{
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    public function setId($input)
    {
        return $this->id;
    }
}