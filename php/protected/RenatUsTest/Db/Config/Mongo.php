<?php

namespace RenatUsTest\Db\Config;

use RenatUsTest\Throws\ErrorExceptions;

use MongoDB\Client;

/**
 * Class Mongo
 * @package RenatUsTest\Db\Config
 * @author Denis Gubenko
 */
class Mongo extends AbstractConfig
{

    /**
     * Connect into database
     * @var \MongoDB\Client|null
     */
    private $connect = null;

    /**
     * @var array|null
     */
    private $config = null;

    /**
     * @var string|null
     */
    private $namespace = null;

    /**
     * @var string|null
     */
    private $url = null;

    /**
     * Pdo constructor.
     * @param array $options
     * @param string|null $namespace
     */
    public function __construct(array $options = [], string $namespace = null)
    {
        $this->setConfig($options, $namespace);
        $this->setConnect();
    }

    /**
     * Set config for database
     * @param array $config
     */
    private function setConfig(array $config, string $namespace = null)
    {
        $this->setupConfig($config);
        $this->checkNamespace($namespace);
        $this->checkParams($config, $namespace);
    }

    /**
     * @param array $input
     */
    private function setupConfig(array $input = []) : void
    {
        $this->config = $input;
    }

    /**
     * @return array
     */
    public function returnConfig() : array
    {
        $returned = $this->config;

        return $returned;
    }

    /**
     * @param string|null $namespace
     * @return bool
     */
    public function checkNamespace(string $namespace = null) :bool
    {
        if (empty($namespace)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Namespace of database was not transmitted in class "%s"',
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setNamespace($namespace);

        return true;
    }

    /**
     * @param string $input
     */
    private function setNamespace(string $input) :void
    {
        $this->namespace = $input;
    }

    /**
     * @return string
     */
    public function getNamespace() :string
    {

        return $this->namespace;
    }

    /**
     * @param array $config
     * @param string|null $namespace
     */
    private function checkParams(array $config, string $namespace = null) :void
    {
        $this->checkUrl($config, $namespace);
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkUrl(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (empty($options['url'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.mongo.%s.driver" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setUrl($options['url']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setUrl(string $input) :void
    {
        $this->url = $input;
    }

    /**
     * @return string
     */
    public function getUrl() :string
    {

        return $this->url;
    }

    /**
     * @return bool
     */
    public function checkConnect() :bool
    {
        if (empty($this->connect) || false === $this->connect instanceof Client) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" connection with the database is not set',
                    __METHOD__
                )
            );
        }

        return true;
    }

    /**
     * @return bool
     */
    public function ping() :bool
    {
        try {
            $this->connect->listDatabases();
        } catch (\Throwable $e) {
            $this->setConnect(); // Don't catch exception here, so that re-connect fail will throw exception
        }

        return true;
    }

    /**
     * Get connection for database
     * @return Client
     */
    public function getConnect() :Client
    {
        $this->checkConnect();
        $this->ping();

        return $this->connect;
    }

    /**
     * Set connection into database
     * @return void
     */
    private function setConnect() :void
    {
        try {
            $this->connect = new Client($this->getUrl());
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error into method "%s" (%s)',
                    __METHOD__,
                    $e->getMessage()
                )
            );
        }
    }

    /**
     * @param array $config
     * @return bool
     */
    public function checkConfig(array $config = []) :bool
    {
        if (empty($config)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Config of database was not transmitted into method "%s"',
                    __METHOD__
                )
            );
        }

        return true;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}