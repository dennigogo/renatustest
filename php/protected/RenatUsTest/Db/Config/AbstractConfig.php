<?php

namespace RenatUsTest\Db\Config;

/**
 * Class AbstractConfig
 * @package RenatUsTest\Db\Config
 * @author Denis Gubenko
 */
abstract class AbstractConfig {

    abstract public function checkConnect() :bool;
    abstract public function checkConfig(array $config = []) :bool;
    abstract public function checkNamespace(string $namespace = null) :bool;
}