<?php

namespace RenatUsTest\Db\Config;

use RenatUsTest\Db\Functions\Common\Pdo\Delete;
use RenatUsTest\Db\Functions\Common\Pdo\Select;
use RenatUsTest\Db\Functions\Common\Pdo\Update;
use RenatUsTest\Db\Functions\Common\Pdo\Insert;
use RenatUsTest\Db\Functions\Common\Pdo\Platform;
use RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject as RenatUsTestSelect;
use RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject as RenatUsTestInsert;
use RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject as RenatUsTestUpdate;
use RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject as RenatUsTestDelete;
use RenatUsTest\Db\Functions\Common\Pdo\Random\RandomObject as RenatUsTestRandom;
use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Throws\ErrorExceptions;

use Zend\Db\Sql\Select as ZendSelect;
use Zend\Db\Sql\Insert as ZendInsert;
use Zend\Db\Sql\Update as ZendUpdate;
use Zend\Db\Sql\Delete as ZendDelete;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Metadata\Metadata;

/**
 * Class Pdo
 * @package RenatUsTest\Db\Config
 * @author Denis Gubenko
 */
class Pdo extends AbstractConfig
{
    
    /**
     * Connect into database
     * @var \Zend\Db\Adapter\Adapter|null
     */
    private $connect = null;

    /**
     * @var \Zend\Db\Metadata\Metadata|null
     */
    private $metadata = null;

    /**
     * @var array|null
     */
    private $config = null;

    /**
     * @var string|null
     */
    private $namespace = null;

    /**
     * @var string|null
     */
    private $driver = null;

    /**
     * @var string|null
     */
    private $dbname = null;

    /**
     * @var string|null
     */
    private $username = null;

    /**
     * @var string|null
     */
    private $password = null;

    /**
     * @var string|null
     */
    private $host = null;

    /**
     * @var string|null
     */
    private $prefix = null;

    /**
     * @var string|null
     */
    private $collation = null;

    /**
     * @var string|null
     */
    private $charset = null;

    /**
     * @var string|null
     */
    private $platform = null;
    
    /**
     * Pdo constructor.
     * @param array $options
     * @param string|null $namespace
     */
    public function __construct(array $options = [], string $namespace = null)
    {
        $this->setConfig($options, $namespace);
        $this->setConnect();
    }
    
    /**
     * Set config for database
     * @param array $config
     */
    private function setConfig(array $config, string $namespace = null)
    {
        $this->setupConfig($config);
        $this->checkNamespace($namespace);
        $this->checkParams($config, $namespace);
    }

    /**
     * @param array $config
     * @param string|null $namespace
     */
    private function checkParams(array $config, string $namespace = null) :void
    {
        $this->checkDriver($config, $namespace);
        $this->checkDbname($config, $namespace);
        $this->checkUsername($config, $namespace);
        $this->checkPassword($config, $namespace);
        $this->checkHost($config, $namespace);
        $this->checkPrefix($config, $namespace);
        $this->checkCollation($config, $namespace);
        $this->checkCharset($config, $namespace);
        $this->checkPlatform($config, $namespace);
    }

    /**
     * @param array $input
     */
    private function setupConfig(array $input = []) : void
    {
        $this->config = $input;
    }

    /**
     * @return array
     */
    public function returnConfig() : array
    {
        $returned = $this->config;

        return $returned;
    }
    
    /**
     * Set connection into database
     * @return void
     */
    private function setConnect() :void
    {
        $config = Arrays::getInstance()->arr2obj($this->returnConfig());
        try {
            $this->connect = new Adapter([
                'driver'   => $this->getDriver(),
                'database' => $this->getDbname(),
                'username' => $this->getUsername(),
                'password' => $this->getPassword(),
                'host' => $this->getHost(),
                'prefix' => $this->getPrefix(),
                'collation' => $this->getCollation(),
                'charset' => $this->getCharset(),
            ]);
            $this->init();
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error into method "%s" (%s)',
                    __METHOD__,
                    $e->getMessage()
                )
            );
        }
    }

    private function init(): void
    {
        Platform::getInstance()->setupPlatform($this);
        $this->setMetadata();
    }

    /**
     * Setup object \Zend\Db\Metadata\Metadata
     * @return void
     */
    private function setMetadata() :void
    {

        $this->metadata = new Metadata($this->getConnect());
    }

    /**
     * Return object \Zend\Db\Metadata\Metadata
     * @return \Zend\Db\Metadata\Metadata
     */
    public function getMetadata() :Metadata
    {
        return $this->metadata;
    }

    /**
     * @param string $table
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject $input
     * @return \Zend\Db\Sql\Select
     */
    public function select(string $table, RenatUsTestSelect $input) :ZendSelect
    {
        $returned = new Select($this->getConnect(), $table, $input);

        return $returned->getParentObject();
    }

    /**
     * @param string $table
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject $input
     * @return \Zend\Db\Sql\Insert
     */
    public function insert(string $table, RenatUsTestInsert $input) :ZendInsert
    {
        $returned = new Insert($this->getConnect(), $table, $input);

        return $returned->getParentObject();
    }

    /**
     * @param RenatUsTestRandom $object
     * @return bool
     */
    public function randomQuery(RenatUsTestRandom $object) :bool
    {
        $returned = false;
        try {
            $t = $this->getConnect()->query($object->getSql());
            $t->execute($object->getItems());
            $object->getItems();
            $returned = true;
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error with method "%s" into class "%s". '
                    . "Info: " . print_r($e->getMessage(), true),
                    __METHOD__,
                    get_class($this)
                )
            );
        }

        return $returned;
    }

    /***
     * @param string $table
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Update\UpdateObject $input
     * @return \Zend\Db\Sql\Update
     */
    public function update(string $table, RenatUsTestUpdate $input) :ZendUpdate
    {
        $returned = new Update($this->getConnect(), $table, $input);

        return $returned->getParentObject();
    }

    /***
     * @param string $table
     * @param \RenatUsTest\Db\Functions\Common\Pdo\Delete\DeleteObject $input
     * @return \Zend\Db\Sql\Delete
     */
    public function delete(string $table, RenatUsTestDelete $input) :ZendDelete
    {
        $returned = new Delete($this->getConnect(), $table, $input);

        return $returned->getParentObject();
    }
    
    /**
     * Get connection for database
     * @return Adapter
     */
    public function getConnect() :Adapter
    {
        $this->checkConnect();
        $this->ping();

        return $this->connect;
    }

    /**
     * @return bool
     */
    public function checkConnect() :bool
    {
        if (empty($this->connect) || false === $this->connect instanceof Adapter) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" connection with the database is not set',
                    __METHOD__
                )
            );
        }

        return true;
    }

    /**
     * @return bool
     */
    public function ping() :bool
    {
        try {
            $this->connect->query('SELECT 1');
        } catch (\Throwable $e) {
            $this->setConnect(); // Don't catch exception here, so that re-connect fail will throw exception
        }

        return true;
    }

    /**
     * @param array $config
     * @return bool
     */
    public function checkConfig(array $config = []) :bool
    {
        if (empty($config)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Config of database was not transmitted into method "%s"',
                    __METHOD__
                )
            );
        }

        return true;
    }

    /**
     * @param string|null $namespace
     * @return bool
     */
    public function checkNamespace(string $namespace = null) :bool
    {
        if (empty($namespace)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Namespace of database was not transmitted in class "%s"',
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setNamespace($namespace);

        return true;
    }

    /**
     * @param string $input
     */
    private function setNamespace(string $input) :void
    {
        $this->namespace = $input;
    }

    /**
     * @return string
     */
    public function getNamespace() :string
    {

        return $this->namespace;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkDriver(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (empty($options['driver'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.driver" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setDriver($options['driver']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setDriver(string $input) :void
    {
        $this->driver = $input;
    }

    /**
     * @return string
     */
    public function getDriver() :string
    {

        return $this->driver;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkDbname(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (empty($options['dbname'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.dbname" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setDbname($options['dbname']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setDbname(string $input) :void
    {
        $this->dbname = $input;
    }

    /**
     * @return string
     */
    public function getDbname() :string
    {

        return $this->dbname;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkUsername(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (!isset($options['username'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.username" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setUsername($options['username']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setUsername(string $input) :void
    {
        $this->username = $input;
    }

    /**
     * @return string
     */
    public function getUsername() :string
    {

        return $this->username;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkPassword(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (!isset($options['password'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.password" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setPassword($options['password']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setPassword(string $input) :void
    {
        $this->password = $input;
    }

    /**
     * @return string
     */
    public function getPassword() :string
    {

        return $this->password;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkHost(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (empty($options['host'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.host" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setHost($options['host']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setHost(string $input) :void
    {
        $this->host = $input;
    }

    /**
     * @return string
     */
    public function getHost() :string
    {

        return $this->host;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkPrefix(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (!isset($options['prefix'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.prefix" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setPrefix($options['prefix']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setPrefix(string $input) :void
    {
        $this->prefix = $input;
    }

    /**
     * @return string
     */
    public function getPrefix() :string
    {

        return $this->prefix;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkCollation(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (empty($options['collation'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.collation" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setCollation($options['collation']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setCollation(string $input) :void
    {
        $this->collation = $input;
    }

    /**
     * @return string
     */
    public function getCollation() :string
    {

        return $this->collation;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkCharset(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (!isset($options['charset'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.charset" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s"',
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setCharset($options['charset']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setCharset(string $input) :void
    {
        $this->charset = $input;
    }

    /**
     * @return string
     */
    public function getCharset() :string
    {

        return $this->charset;
    }

    /**
     * @param array $options
     * @param string|null $namespace
     * @return bool
     */
    private function checkPlatform(array $options = [], string $namespace = null) :bool
    {
        $this->checkNamespace($namespace);
        if (!isset($options['platform'])) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Variable "db.pdo.%s.platform" '
                    . 'from the file "%sdb.ini" was not transmitted in class "%s". '
                    . 'Expected values list: ' . print_r([
                        'Zend\Db\Adapter\Platform\Mysql',
                        'Zend\Db\Adapter\Platform\Postgresql',
                        'Zend\Db\Adapter\Platform\Oracle',
                        'Zend\Db\Adapter\Platform\IbmDb2',
                        'Zend\Db\Adapter\Platform\Sql92',
                        'Zend\Db\Adapter\Platform\Sqlite',
                        'Zend\Db\Adapter\Platform\SqlServer'
                    ], true),
                    $namespace,
                    INI_PATH,
                    get_class($this)
                )
            );
        }
        $this->setPlatform($options['platform']);

        return true;
    }

    /**
     * @param string $input
     */
    private function setPlatform(string $input) :void
    {
        $this->platform = $input;
    }

    /**
     * @return string
     */
    public function getPlatform() :string
    {

        return $this->platform;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}