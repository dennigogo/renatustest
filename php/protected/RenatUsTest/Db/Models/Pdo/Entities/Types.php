<?php

namespace RenatUsTest\Db\Models\Pdo\Entities;

/**
 * Class Types
 * @package RenatUsTest\Db\Models\Pdo\Entities
 * @author Denis Gubenko
 */
class Types
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $type;

    /**
     * @return mixed
     */
    public function getId() :int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}