<?php

namespace RenatUsTest\Db\Models\Pdo\Entities;

/**
 * Class ImportsTimestamp
 * @package RenatUsTest\Db\Models\Pdo\Entities
 * @author Denis Gubenko
 */
class ImportsTimestamp
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $created;

    /**
     * @return mixed
     */
    public function getId() :int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getCreated() :float
    {
        return $this->created;
    }

    /**
     * @param mixed $createdFull
     */
    public function setCreated($created): void
    {
        $this->created = $created;
    }
}