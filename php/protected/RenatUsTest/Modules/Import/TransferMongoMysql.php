<?php

namespace RenatUsTest\Modules\Import;

use RenatUsTest\Config\Kernel;
use RenatUsTest\Config\Db;
use RenatUsTest\Db\Functions\Common\Pdo\Select\SelectObject as RenatUsTestSelect;
use RenatUsTest\Db\Functions\Common\Pdo\Random\RandomObject as RenatUsTestRandom;
use RenatUsTest\Db\Functions\Common\Pdo\Insert\InsertObject as RenatUsTestInsert;
use RenatUsTest\Db\Functions\Mongo as MongoFunction;
use RenatUsTest\Db\Functions\Pdo as PdoFunction;
use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Helpers\Strings;
use RenatUsTest\Modules\Common\AbstractModules;
use RenatUsTest\Throws\ErrorExceptions;

use MongoDB\Driver\Query as MongoDBQuery;
use MongoDB\Driver\Cursor as MongoDBCursor;

/**
 * Class TransferMongoMysql
 * @package RenatUsTest\Modules\Import
 * @author Denis Gubenko
 */
class TransferMongoMysql extends AbstractModules
{
    /**
     * @var \RenatUsTest\Db\Functions\Mongo|null
     */
    private $connectMongo = null;
    /**
     * @var \RenatUsTest\Db\Functions\Pdo|null
     */
    private $connectMysql = null;

    /**
     * @var string
     */
    private $namespaceMongo = self::defaultNamespaceMongo;

    /**
     * @var string
     */
    private $namespaceMysql = self::defaultNamespaceMysql;

    /**
     * @var string|null
     */
    private $dbNameMongo = null;

    /**
     * @var string|null
     */
    private $dbNameMysql = null;

    /**
     * @var array|null
     */
    private $typesList = null;

    /**
     * @var array|null
     */
    private $importsTimestamp = [];

    /**
     * @var null
     */
    private $mongoQuery = null;

    /**
     *
     */
    private const defaultSql = [
        ' INSERT IGNORE INTO info ( %s ) ',
        ' VALUES ',
        ' ( %s )'
    ];

    /**
     * TransferMongoMysql constructor.
     * @param string|null $namespaceMongo
     * @param string|null $namespaceMysql
     * @throws \ReflectionException
     */
    public function __construct(string $namespaceMongo = null, string $namespaceMysql = null)
    {
        $this->init($namespaceMongo, $namespaceMysql);
    }

    /**
     * @param string|null $namespaceMongo
     * @param string|null $namespaceMysql
     * @throws \ReflectionException
     */
    private function init(string $namespaceMongo = null, string $namespaceMysql = null) :void
    {
        Kernel::getInstance()->buildKernel();
        $namespaceMongo =
            (false === Strings::getInstance()->emptyValidator($namespaceMongo))
                ? $namespaceMongo : (string) self::defaultNamespaceMongo;
        $this->setNamespaceMongo($namespaceMongo);
        $namespaceMysql =
            (false === Strings::getInstance()->emptyValidator($namespaceMysql))
                ? $namespaceMysql : (string) self::defaultNamespaceMysql;
        $this->setNamespaceMysql($namespaceMysql);
        $this->checkConnectMongo();
        $this->checkConnectMysql();
        $this->checkDbNameMongo();
        $this->checkDbNameMysql();
        $this->setTypesList();
        $this->setImportsTimestamp();
    }

    private function checkConnectMongo() :void
    {
        $connect = Db::getInstance()->getConnection($this->getNamespaceMongo());
        if (empty($connect) || false === $connect instanceof MongoFunction) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" connection with the database is not set',
                    __METHOD__
                )
            );
        }
        $this->setConnectMongo($connect);
    }

    /**
     * @param \RenatUsTest\Db\Functions\Mongo $connect
     */
    private function setConnectMongo(MongoFunction $connect) :void
    {
        $this->connectMongo = $connect;
    }

    /**
     * @return \RenatUsTest\Db\Functions\Mongo
     */
    public function getConnectMongo() :MongoFunction
    {

        return $this->connectMongo;
    }

    private function checkConnectMysql() :void
    {
        $connect = Db::getInstance()->getConnection($this->getNamespaceMysql());
        if (empty($connect) || false === $connect instanceof PdoFunction) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" connection with the database is not set',
                    __METHOD__
                )
            );
        }
        $this->setConnectMysql($connect);
    }

    /**
     * @param \RenatUsTest\Db\Functions\Pdo $connect
     */
    private function setConnectMysql(PdoFunction $connect) :void
    {
        $this->connectMysql = $connect;
    }

    /**
     * @return \RenatUsTest\Db\Functions\Pdo
     */
    public function getConnectMysql() :PdoFunction
    {

        return $this->connectMysql;
    }

    private function checkDbNameMongo() :void
    {
        try {
            if (!empty(
            Arrays::getInstance()->arr2obj(
                $this->getConnectMongo()->getConfig()->returnConfig()
            )->dbname
            )) {
                $this->setDbNameMongo(
                    $this->dbName = Arrays::getInstance()->arr2obj(
                        $this->getConnectMongo()->getConfig()->returnConfig()
                    )->dbname
                );
            }
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" into class "%s" database name not set.',
                    __METHOD__,
                    get_class($this)
                )
            );
        }
    }

    private function setDbNameMongo(string $dbname) :void
    {
        $this->dbNameMongo = $dbname;
    }

    /**
     * @return string
     */
    private function getDbNameMongo() :string
    {
        return $this->dbNameMongo;
    }

    private function checkDbNameMysql() :void
    {
        try {
            if (!empty(
            Arrays::getInstance()->arr2obj(
                $this->getConnectMysql()->getConfig()->returnConfig()
            )->dbname
            )) {
                $this->setDbNameMysql(
                    $this->dbName = Arrays::getInstance()->arr2obj(
                        $this->getConnectMysql()->getConfig()->returnConfig()
                    )->dbname
                );
            }
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" into class "%s" database name not set.',
                    __METHOD__,
                    get_class($this)
                )
            );
        }
    }

    /**
     * @param string $dbname
     */
    private function setDbNameMysql(string $dbname) :void
    {
        $this->dbNameMysql = $dbname;
    }

    /**
     * @return string
     */
    private function getDbNameMysql() :string
    {
        return $this->dbNameMysql;
    }

    /**
     * @param string $input
     */
    private function setNamespaceMongo(string $input) :void
    {
        $this->namespaceMongo = $input;
    }

    /**
     * @return string
     */
    public function getNamespaceMongo() :string
    {

        return $this->namespaceMongo;
    }

    /**
     * @param string $input
     */
    private function setNamespaceMysql(string $input) :void
    {
        $this->namespaceMysql = $input;
    }

    /**
     * @return string
     */
    public function getNamespaceMysql() :string
    {

        return $this->namespaceMysql;
    }

    private function setTypesList() :void
    {
        $typesList = [];
        $result = $this->getConnectMysql()->types->select(
            (new RenatUsTestSelect())
                ->setColumns(['id', 'type'])
                ->setOrder(['id' => "ASC"])

        );
        if (empty($result) || !is_array($result)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'Critical error. Types list with method "%s" does not exist for "%s" class',
                    __METHOD__,
                    get_class($this)
                )
            );
        }
        foreach ($result as $item) {
            $typesList[$item->id] = $item->type;
        }
        $this->typesList = $typesList;
    }

    /**
     * @return array
     */
    public function getTypesList() :array
    {

        return $this->typesList;
    }

    private function setImportsTimestamp(): void
    {
        $importsTimestamp = $this->getConnectMysql()->importsTimestamp->select(
            (new RenatUsTestSelect())
                ->setColumns(['created'])
                ->setOrder(['created' => "DESC"])
                ->setLimit(1)
        );
        $this->importsTimestamp = (!empty($importsTimestamp) && is_array($importsTimestamp)) ? $importsTimestamp : [];
    }

    /**
     * @return array
     */
    private function getImportsTimestamp() :array
    {
        return $this->importsTimestamp;
    }

    /**
     * @param array|object $filter The search filter.
     * @param array $options
     * @throws InvalidArgumentException on argument parsing errors.
     */
    private function setMongoQuery($filter, array $options = []) :void
    {
        $this->mongoQuery = new MongoDBQuery($filter, $options);
    }

    /**
     * @return \MongoDB\Driver\Query
     */
    private function getMongoQuery() :MongoDBQuery
    {

        return $this->mongoQuery;
    }

    /**
     * @param string $collection
     * @param MongoDBQuery $query
     * @return MongoDBCursor
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function executeQueryMongo(string $collection, MongoDBQuery $query) :MongoDBCursor
    {
        $cursor = $this->getConnectMongo()->executeQuery($collection, $query);

        return $cursor;
    }

    /**
     * @return bool
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function transfer() :bool
    {
        $returned = false;

        // init random Mysql class
        $randomMysql = new RenatUsTestRandom();

        // check exists import's report
        $firstStart = (empty($this->getImportsTimestamp())) ? true : false;

        // default settings with MongoDb Query
        $options['sort'] = ['_id' => 1];

        // init default variables
        $go = true;
        $first = null;
        $next = null;
        $iteratorCount = null;

        $filter = [];
        $filter['created'] = [];

        $minTimestamp = 0;

        $microtime = (string) microtime(true);
        $input = explode(".", $microtime);
        $input[1] = (empty($input[1])) ? 0 : $input[1];
        $msInt = substr($input[1], 0, 3) + 0;
        $ms = str_pad($msInt, 3, "0", STR_PAD_RIGHT);
        $maxTimestamp = ((new \DateTime('UTC'))->getTimestamp()  . '.' . $ms) + 0;
        if (!empty($this->getImportsTimestamp())) {
            $minTimestamp = $this->getImportsTimestamp()[0]->created;
        }
        $filter['created'] = [ '$gt' => $minTimestamp, '$lt' => $maxTimestamp ];
        $this->setMongoQuery($filter, $options);
        $items = $this->executeQueryMongo('info', $this->getMongoQuery());
        $result = $items->toArray();
        if (!empty($result)) {
            $values = [];
            $sqlValues = [];
            $sqlStart = false;
            foreach ($result as $item) {
                unset($item->_id);
                $item->datetime = (new \DateTime('UTC'))->setTimestamp($item->timestamp)->format("Y-m-d H:i:s");
                if (false === $sqlStart) {
                    $sql = sprintf(self::defaultSql[0], join(', ', array_keys(Arrays::getInstance()->obj2arr($item))));
                    $sqlStart = true;
                }
                $minTimestamp = $item->created + 0;
                $itemValue = Arrays::getInstance()->obj2arr($item);
                array_push(
                    $sqlValues,
                    sprintf(self::defaultSql[2], join(', ', array_fill_keys(array_keys($itemValue), '?')))
                );

                foreach ($itemValue as $key => $value) {
                    $value = ('type' === $key) ? array_search($value, $this->getTypesList()) : $value;
                    array_push($values, $value);
                }
            }
            $sql .= self::defaultSql[1] . join(', ', $sqlValues);
            $randomMysql->setSql($sql);
            $randomMysql->setItems($values);
            $this->getConnectMysql()->info->randomQuery($randomMysql);
        }
        $this->getConnectMysql()->importsTimestamp->insert(
            (new RenatUsTestInsert())
                ->setColumns( [ 'created' ] )
                ->setValues( [ $maxTimestamp ] )
        );
        $returned = true;

        return $returned;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}