<?php

namespace RenatUsTest\Modules\Faker;

use RenatUsTest\Config\Kernel;
use RenatUsTest\Config\Db;
use RenatUsTest\Db\Functions\Mongo as MongoFunction;
use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Helpers\Strings;
use RenatUsTest\Modules\Common\AbstractModules;
use RenatUsTest\Throws\ErrorExceptions;

use Faker\Factory;
use Faker\Generator;

/**
 * Class InitMongoCollectionInfo
 * @package RenatUsTest\Modules\Faker
 * @author Denis Gubenko
 */
class InitMongoCollectionInfo extends AbstractModules
{
    /**
     * @var \RenatUsTest\Db\Functions\Mongo|null
     */
    private $connect = null;

    /**
     * @var string
     */
    private $namespace = self::defaultNamespaceMongo;

    /**
     * @var string|null
     */
    private $dbName = null;

    /**
     * @var \Faker\Generator|null
     */
    private $faker = null;

    /**
     * @var array
     */
    private $ts = [];

    /**
     * @var array
     */
    private $tt = [];

    /**
     * @var array
     */
    private $uid = [];

    /**
     * @var array
     */
    private $uidTt = [];

    /**
     * InitMongoCollectionInfo constructor.
     * @param string|null $namespace
     */
    public function __construct(string $namespace = null)
    {
        $this->init($namespace);
    }

    /**
     * @param string|null $namespace
     */
    private function init(string $namespace = null) :void
    {
        Kernel::getInstance()->buildKernel();
        $namespace =
            (false === Strings::getInstance()->emptyValidator($namespace))
                ? $namespace : (string) self::defaultNamespaceMongo;
        $this->setNamespace($namespace);
        $this->checkConnect();
        $this->checkDbName();
        $this->initParams();
    }

    private function initParams() :void
    {
        $this->initTs();
        $this->initTt();
        $this->initUid();
    }

    private function initTs() :void
    {
        array_push($this->ts, join("_", Factory::create()->words));
        array_push($this->ts, join("_", Factory::create()->words));
        array_push($this->ts, join("_", Factory::create()->words));
        array_push($this->ts, join("_", Factory::create()->words));
        array_push($this->ts, join("_", Factory::create()->words));
    }

    private function initTt() :void
    {
        array_push($this->tt, join("_", Factory::create()->words));
        array_push($this->tt, join("_", Factory::create()->words));
        array_push($this->tt, join("_", Factory::create()->words));
        array_push($this->tt, join("_", Factory::create()->words));
        array_push($this->tt, join("_", Factory::create()->words));
    }

    private function initUid() :void
    {
        array_push(
            $this->uid,
            Factory::create()->unique()->firstName
            . Factory::create()->unique()->firstName . Factory::create()->unique()->firstName
        );
        array_push(
            $this->uid,
            Factory::create()->unique()->firstName
            . Factory::create()->unique()->firstName . Factory::create()->unique()->firstName
        );
        array_push(
            $this->uid,
            Factory::create()->unique()->firstName
            . Factory::create()->unique()->firstName . Factory::create()->unique()->firstName
        );
        array_push(
            $this->uid,
            Factory::create()->unique()->firstName
            . Factory::create()->unique()->firstName . Factory::create()->unique()->firstName
        );
        array_push(
            $this->uid,
            Factory::create()->unique()->firstName
            . Factory::create()->unique()->firstName . Factory::create()->unique()->firstName
        );
    }

    private function checkConnect() :void
    {
        $connect = Db::getInstance()->getConnection($this->getNamespace());
        if (empty($connect) || false === $connect instanceof MongoFunction) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" connection with the database is not set',
                    __METHOD__
                )
            );
        }
        $this->setConnect($connect);
    }

    /**
     * @param \RenatUsTest\Db\Functions\Mongo $connect
     */
    private function setConnect(MongoFunction $connect) :void
    {
        $this->connect = $connect;
    }

    /**
     * @return \RenatUsTest\Db\Functions\Mongo
     */
    public function getConnect() :MongoFunction
    {

        return $this->connect;
    }

    private function checkDbName() :void
    {
        try {
            if (!empty(
                Arrays::getInstance()->arr2obj(
                    $this->getConnect()->getConfig()->returnConfig()
                )->dbname
            )) {
                $this->setDbName(
                    $this->dbName = Arrays::getInstance()->arr2obj(
                        $this->getConnect()->getConfig()->returnConfig()
                    )->dbname
                );
            }
        } catch (\Throwable $e) {
            throw ErrorExceptions::showThrow(
                sprintf(
                    'Critical error. Into method "%s" into class "%s" database name not set.',
                    __METHOD__,
                    get_class($this)
                )
            );
        }
    }

    private function setDbName(string $dbname) :void
    {
        $this->dbName = $dbname;
    }

    /**
     * @return string
     */
    private function getDbName() :string
    {
        return $this->dbName;
    }

    /**
     * @param string $input
     */
    private function setNamespace(string $input) :void
    {
        $this->namespace = $input;
    }

    /**
     * @return string
     */
    public function getNamespace() :string
    {

        return $this->namespace;
    }

    /**
     * @param string $dbName
     * @param string $collection
     * @param int|null $addDays
     * @throws \Exception
     */
    public function populateInfo(string $collection, int $addDays = null, string $dbName = null) :void
    {
        $limit = 10000;
        $objectArray = [];
        if (false === Strings::getInstance()->emptyValidator($dbName)) {
            $this->setDbName($dbName);
        }
        if (false === Strings::getInstance()->emptyValidator($collection)) {
            $endTime = (new \DateTime(null, new \DateTimeZone("UTC")));
            $startTime = clone $endTime;
            if (!is_null($addDays) && 0 > $addDays) {
                $startTime->sub(new \DateInterval("P" . abs($addDays) . "D"));
            } elseif (is_null($addDays) || 0 == $addDays) {
                $startTime->sub(new \DateInterval("P1D"));
            } else {
                $endTime->add(new \DateInterval("P" . $addDays . "D"));
            }
            $endTimestamp = $endTime->getTimestamp() + 0;
            $this->getConnect()->drop($collection, $this->getDbName());
            $go = true;
            while (false !== $go) {
                // setup faker Class
                $this->createFaker();
                // setup timestamps
                $startTime->add(new \DateInterval("PT5M"));
                $startTimestamp = $startTime->getTimestamp() + 0;
                // setup object with type 'install'
                $object = new \stdClass();
                $object->timestamp = $startTimestamp;
                $object = $this->populateObject($object, 0);
                $idUid = $object->uid . '-' . self::defaultType[0] . '-' . $object->tt;
                if (!isset($this->uidTt[$idUid])) {
                    $this->uidTt[$idUid] = true;
                    array_push($objectArray, clone $object);
                }
                if ($object->type === self::defaultType[0] && false !== (bool)random_int(0, 1)) {
                    // setup object with type 'login'
                    $object->timestamp = $startTimestamp;
                    $object = $this->populateObject($object, 1);
                    array_push($objectArray, clone $object);
                    if ($object->type === self::defaultType[1] && false !== (bool)random_int(0, 1)) {
                        // setup object with type 'end'
                        $object->timestamp = $startTimestamp;
                        $object = $this->populateObject($object, 2);
                        array_push($objectArray, clone $object);
                    }
                }
                // check stop while
                if ($startTimestamp > $endTimestamp) {
                    $go = false;
                }
                if ($limit >= count($objectArray)) {
                    $this->getConnect()->insertMany($collection, $objectArray, $this->getDbName());
                    $objectArray = [];
                }
            }
        }
    }

    private function createFaker()
    {
        $this->faker = Factory::create();
    }

    /**
     * @return \Faker\Generator
     */
    private function returnFaker() :Generator
    {
        return $this->faker;
    }

    /**
     * @param \stdClass $object
     * @param int $counterType
     * @return \stdClass
     * @throws \Exception
     */
    private function populateObject(\stdClass $object, int $counterType) :\stdClass
    {
        switch ($counterType) {
            case 0:
                $object->uid = $this->uid[random_int(0, count($this->uid) - 1)];
                $object->tt = $this->tt[random_int(0, count($this->tt) - 1)];
                $object->ts = $this->ts[random_int(0, count($this->ts) - 1)];
            default:
                $object->type = self::defaultType[$counterType];
                $microtime = (string) microtime(true);
                $input = explode(".", $microtime);
                $input[1] = (empty($input[1])) ? 0 : $input[1];
                $input[1] = $this->setupMs($input[1]);
                $object->created = ($object->timestamp . "." . $input[1]) + 0;
                $object->timestamp = $object->timestamp + 0;
                $object->ms = $input[1] + 0;
                break;
        }

        return $object;
    }

    /**
     * @param string $ms
     * @return string
     */
    private function setupMs(string $ms) :string
    {
        $ms = (empty($ms)) ? 0 : $ms;
        $msInt = substr($ms, 0, 3) + 0;
        $ms = str_pad($msInt, 3, "0", STR_PAD_RIGHT);

        return $ms;
    }

    /**
     * The handler functions that do not exist
     * @param $method
     * @param $args
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw ErrorExceptions::getInstance()->getMessage(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
        }
    }
}