<?php

namespace RenatUsTest\Modules\Common;

/**
 * Class AbstractModules
 * @package RenatUsTest\Modules\Common
 * @author Denis Gubenko
 */
class AbstractModules
{
    /**
     * @var string
     */
    protected const defaultNamespaceMongo = 'mongodb';
    /**
     * @var string
     */
    protected const defaultNamespaceMysql = 'mysql';

    /**
     * @var array
     */
    protected const defaultType = [ 'install', 'login', 'end' ];
}