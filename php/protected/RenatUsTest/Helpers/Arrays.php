<?php

namespace RenatUsTest\Helpers;

use RenatUsTest\Throws\ErrorException;

/**
 * Class Arrays
 * @package RenatUsTest\Helpers
 * @author Denis Gubenko
 */
class Arrays
{
	/**
     * Singleton instance
     * @var \RenatUsTest\Helpers\Arrays|null
     */
    protected static $instance = null;
    
    /**
     * Arrays constructor.
     */
	protected function __construct(){
	    return $this;
    }

	/**
     * Singleton pattern implementation makes "clone" unavailable
     * @return void
     */
    protected function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Helpers\ArrayHelpers
     * Singleton pattern implementation
     * @return null|Arrays
     */
    public static function getInstance()
    {
    	if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns stdClass() from Array()
     * @param array $options
     * @param int|null $keyType (null: key's array as is; 0: key's array as lower case; 1: key's array as upper case;)
     * @return \stdClass
     */
    public function arr2obj(array $options = [], int $keyType = null) :\stdClass
    {
    	if (is_array($options)) {
    		if (!is_null($keyType) && !$keyType) {
    			$options = array_change_key_case($options, CASE_LOWER);
    		} elseif (!is_null($keyType) && $keyType) {
    			$options = array_change_key_case($options, CASE_UPPER);
    		}
    		foreach ($options as $key => $item) {
    			if (is_array($item)) {
    				$options[$key] = self::arr2obj($item, $keyType);
    			}
    		}
    	} else {
    		$options = [];
    	}

    	return (object) $options;
    }

    /**
     * Returns Array() from stdClass()
     * @param \stdClass|null $options
     * @param int|null $keyType null: key's stdClass as is; 0: key's stdClass as lower case; 1: key's stdClass as upper case
     * @return array|null|stdClass
     */
    public function obj2arr($options = null, $keyType = null)
    {
    	if ($options instanceof \stdClass) {
    		$options = (array)$options;
    		if (!is_null($keyType) && !$keyType) {
    			$options = array_change_key_case($options, CASE_LOWER);
    		} elseif(!is_null($keyType) && $keyType) {
    			$options = array_change_key_case($options, CASE_UPPER);
    		}
    		foreach ($options as $key => $item) {
    			if ($item instanceof \stdClass) {
    				$options[$key] = self::obj2arr($item, $keyType);
    			}
    		}
    	} else {
    		$options = new \stdClass();
    	}

    	return $options;
    }

    /**
     * Returns an array with the keys of the lower case
     * @param mixed $options
     * @return array|null
     */
    public function arrayAllKeysToLowerCase($options = null)
    {
    	if (!empty($options) && is_array($options)) {
    		$options = array_change_key_case($options, CASE_LOWER);
    		foreach ($options as $key => $item) {
    			if (!empty($item) && is_array($item)) {
					$options[$key] = self::arrayAllKeysToLowerCase($item);
    			}
    		}
    	} else {
    		$options = [];
    	}

    	return $options;
    }

    /**
     * Returns an array with the keys of the upper case
     * @param mixed $options
     * @return array|null
     */
    public function arrayAllKeysToUpperCase($options = null)
    {
    	if (!empty($options) && is_array($options)) {
    		$options = array_change_key_case($options, CASE_UPPER);
    		foreach ($options as $key => $item) {
    			if (is_array($item)) {
    				$options[$key] = self::arrayAllKeysToUpperCase($item);
    			}
    		}
    	} else {
    		$options = [];
    	}

    	return $options;
    }

    /**
     * Split a string by string as stdClass() or Array()
     *
     * @param string|null $string
     * @param string|null $delimiter
     * @param bool $return
     * @return array
     */
    public function explode($string = null, $delimiter = null, $return = false)
    {
    	$result = [];
    	if(
    	    !empty($string) && is_string($string)
    	    && !empty($delimiter) && is_string($delimiter)
        ){
    		$result = explode($delimiter, $string);
	    	if (!empty($result) && is_array($result)) {
    			if(false !== (bool) $return){
    				$result = self::arr2obj($result);
    			}
	    	}
    	}

    	return $result;
    }

    /**
     * @param \stdClass|array|null $array
     * @param string|null $delimiter
     * @param bool $return
     * @return array|object
     */
    public function implodeKeys($array = null, $delimiter = null, $return = false)
    {
    	$result = [];
    	if ($array instanceof \stdClass) {
    		$array = self::obj2arr($array);
    	}
    	if (!empty($array) && is_array($array)) {
    		foreach ($array as $key => $item) {
    			if ($item instanceof \stdClass) {
    				$item = self::obj2arr($item);
	    		}
	    		if (is_array($item)) {
	    			foreach (self::implodeKeys($item, $delimiter) as $key2 => $item2) {
	    			    $keyItem = $key . $delimiter . $key2;
	    				$result[$keyItem] = $item2;
	    			}
	    		} else {
	    			$result[$key] = $item;
	    		}
    		}
    	}
    	if (false !== (bool) $return) {
    		$result = self::arr2obj($result);
    	}

    	return $result;
    }

    /**
     * Elements of the Array(stdClass) in turn key multi-dimensional Array(stdClass)
     *
     * $options - the input array()
     * $item - assigned value to key multi-dimensional Array(stdClass)
     * $reverse:
     *  - NULL : Array(stdClass) as is;
     *  - 0    : Array(stdClass) as is;
     *  - 1    : Array(stdClass) as reverse;
     * $key_type:
     *  - NULL : key's Array(stdClass) as is;
     *  - 0    : key's Array(stdClass) as lower case;
     *  - 1    : key's Array(stdClass) as upper case;
     * $return:
     * - NULL : return Array();
     * - 0    : return Array();
     * - 1    : return stdClass();
     * @var string
     * @return Array() or stdClass()
     */
    public function items2MultiDimensionalKeys(
        $options = null, $item = null, $reverse = null, $keyType = null, $return = false
    )
    {
        if (!is_null($reverse) && false !== (bool) $reverse) {
            $options = array_reverse($options);
        }
        for ($i=(count($options)-1); $i>=0; $i--) {
            if ($i==(count($options)-1)) {
                $result = [ $options[$i] => $item ];
            }else{
                $result = [ $options[$i] => $result ];
            }
        }
        $options = $result;
        unset($result);
        if (!is_null($keyType) && !$keyType) {
            $options = self::arrayAllKeysToLowerCase($options);
        } elseif(!is_null($keyType) && $keyType) {
            $options = self::arrayAllKeysToUpperCase($options);
        }
        if (!is_null($return) && false !== (bool) $return) {
            $options = self::arr2obj($options);
        }

        return $options;
    }

    /**
     * @param $method
     * @param $args
     */
	public function __call($method, $args)
    {
    	if (!method_exists($this, $method)) {
         	throw ErrorException::getInstance()->getMessage(
         	    sprintf(
         	        'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
       	} 	
    }
}