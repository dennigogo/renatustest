<?php

namespace RenatUsTest\Helpers;

/**
 * Class Strings
 * @package RenatUsTest\Helpers
 * @author Denis Gubenko
 */
class Strings
{
    /**
     * Singleton instance
     * @var \RenatUsTest\Helpers\Strings|null
     */
    protected static $instance = null;

    /**
     * Strings constructor.
     */
    protected function __construct(){
        return $this;
    }

    /**
     * Singleton pattern implementation makes "clone" unavailable
     * @return void
     */
    protected function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Helpers\Strings
     * Singleton pattern implementation
     * @return null|Strings
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @param $input
     * @return bool|null|string|string[]
     */
    public function trimFilter($input)
    {
        if (is_string($input) || is_numeric($input)) {
            $input = $input . '';
            $return = preg_replace( "/(^\s+)|(\s+$)/us", "", $input);
        }
        $return = (isset($return) && '0' === $return || !empty($return)) ? $return : false;

        return $return;
    }

    /**
     * @param $input
     * @return bool
     */
    public function emptyValidator($input)
    {
        $input = self::trimFilter($input);
        $return = (false === $input || '' == $input) ? true : false;

        return $return;
    }
}