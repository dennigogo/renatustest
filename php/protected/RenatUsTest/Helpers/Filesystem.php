<?php

namespace RenatUsTest\Helpers;

use RenatUsTest\Throws\ErrorException;

/**
 * Class Filesystem
 * @package RenatUsTest\Helpers
 * @author Denis Gubenko
 */
class Filesystem
{

    /**
     * @var \RenatUsTest\Helpers\Filesystem|null
     */
    protected static $instance = null;
    
    /**
     * Filesystem constructor.
     */
	protected function __construct(){

	    return $this;
    }

	/**
     * Singleton pattern implementation makes "clone" unavailable
     * @return void
     */
    protected function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Helpers\Filesystem
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Helpers\Filesystem Provides a fluent interface
     */
    public static function getInstance()
    {
    	if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Returns contents of the directory
     *
     * $options:
     * - NULL       : return contens of the path $_SERVER["DOCUMENT_ROOT"];
     * - array()    : returns content's paths from array();
     * - stdClass() : returns content's paths from stdClass();
     * - string     : returns content's paths from string;
     * 
     * $sorting:
     *  - NULL : the sorted order is alphabetical in ascending order;
     *  - 0    : the result is unsorted;
     *  - 1    : the sort order is alphabetical in descending order;
     * 
     * $key_type:
     *  - NULL : key's array() or stdClass() as is;
     *  - 0    : key's array() or stdClass() as lower case;
     *  - 1    : key's array() or stdClass() as upper case;
     * 
     * @return NULL or Array(type: 0 - is not dir / 1 - is dir; item: path or filename)
     */
    public function getScanDir($options = null, $sorting = null, $keyType = null)
    {
    	if (is_array($options)) {
    		if (!is_null($keyType) && !$keyType) {
    			$options = Arrays::getInstance()->arrayAllKeysToLowerCase($options);
    		} elseif(!is_null($keyType) && $keyType) {
    			$options = Arrays::getInstance()->arrayAllKeysToUpperCase($options);
    		}
    		foreach ($options as $key => $item) {
    			if (!empty($item) && is_array($item)) {
    				$options[$key] = self::getScanDir($item, $sorting, $keyType);
    			}
    		}
    	} elseif($options instanceof stdClass) {
    		$options = self::getScanDir(Arrays::getInstance()->obj4arr($options, $keyType), $sorting, $keyType);
    	} elseif(is_string($options)) {
    		if (!is_null($sorting) && !$sorting && is_dir($options)) {
    			$options = scandir($options, SCANDIR_SORT_NONE);
    		} elseif(!is_null($sorting) && $sorting && is_dir($options)) {
    			$options = scandir($options, SCANDIR_SORT_DESCENDING);
    		} elseif(is_dir($options)) {
    			$options = scandir($options);
    		} else {
    			$options = null;
    		}
    		if (!is_null($options) && is_array($options)) {
    			$result = [];
    			foreach ($options as $item) {
    				if ($item!='.' && $item!='..' && is_dir($item)) {
    					array_push($result, [ 'type' => 1, 'item' => $item ]);
    				} elseif($item!='.' && $item!='..') {
    					array_push($result, [ 'type' => 0, 'item' => $item ]);
    				}
    			}
    			if (count($result)) {
    				$options = $result;
    			} else {
    				$options = null;
    			}
    			unset($result);
    		} else {
    			$options = null;
    		}
    	} else {
    		$options = $_SERVER["DOCUMENT_ROOT"];
    	}

    	return $options;
    }

    /**
     * Returns directory
     * 
     * $options - path for validation
     * @var String() 
     * @return String()
     */
    public static function pathValidator($options = null)
    {
    	if (is_null($options) && !is_string($options) && !strlen(trim($options))) {
    		throw ErrorException::showThrow(
    		    sprintf(
    		        'Critical error. Path was not transmitted into "%s"',
                    __METHOD__
                )
            );
    	}
    	$options = preg_replace('/[\/\\\\]+/', DIRECTORY_SEPARATOR, $options);
    	$options = array_filter(explode(DIRECTORY_SEPARATOR, $options), 'strlen');
    	$return = [];
    	foreach ($options as $key => $item) {
    		$item = trim($item);
    		if ($item != '.') {
    			if ($item == '..') {
    				array_pop($return);
    			} else {
    				$return[] = $item;
    			}
    		}
    	}
    	$options = implode(DIRECTORY_SEPARATOR,$return);
    	unset($return);
    	return (
    	    (strtoupper(substr(php_uname('s'), 0,3))==='WIN')
                ? $options . DIRECTORY_SEPARATOR
                : DIRECTORY_SEPARATOR . $options . DIRECTORY_SEPARATOR
        );
    }

    /**
     * Create directory
     * 
     * $options - path for create
     * @var String() 
     * @return String()
     */
    public static function pathCreate($options = null)
    {
    	if (is_null($options) && !is_string($options) && !strlen(trim($options))) {
    		throw ErrorException::showThrow(
    		    sprintf(
    		        'Critical error. Path was not transmitted into "%s"',
                    __METHOD__
                )
            );
    	}
    	if (!is_dir($options=self::pathValidator($options)) && !mkdir($options,'755',true)) {
    		throw ErrorException::showThrow(
    		    sprintf(
    		        'Critical error. You do not have sufficient rights to create a path "%s"',
                    $options
                )
            );
    	}

    	return $options;
    }

	/**
     * 
     * The handler functions that do not exist
     * 
     * @return Exeption, default - NULL
     * 
     */
	public function __call($method, $args) 
    {
    	if(!method_exists($this, $method)) { 
         	throw ErrorException::getInstance()->getMessage(
         	    sprintf(
         	        'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
       	} 	
    }
/***************************************************************/
}