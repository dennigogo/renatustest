<?php

namespace RenatUsTest\Config;

use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Throws\ErrorException;
use RenatUsTest\Helpers\Filesystem;

/**
 * Class Settings
 * @package RenatUsTest\Config
 * @author Denis Gubenko
 */
class Settings
{
	/**
     * Singleton instance
     * 
     * @var \RenatUsTest\Config\Settings|null
     */
    protected static $instance = null;
    
    /**
     * General variable
     *
     * @var Array()
     */
    protected $settings = null;

    /**
     * Settings constructor.
     */
	private function __construct(){

		return $this;
	}

	/**
     * Singleton pattern implementation makes "clone" unavailable
     *
     * @return void
     */
    protected function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Config\Settings
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Config\Settings Provides a fluent interface
     */
    public static function getInstance()
    {
    	if (null === self::$instance) {
            self::$instance = new self();
            self::$instance->setGeneric();
        }

        return self::$instance;
    }

    /**
     * Reset instance of \RenatUsTest\Config\Settings
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Config\Settings Provides a fluent interface
     */
    public static function resetInstance()
    {
        return self::$instance = null;
    }

    /**
     * Returns Configuration Settings
     *
     * @return stdClass
     */
    public function getGeneric()
    {
    	if(!is_array($this->settings)){
    		self::setGeneric();
    	}
    	return $this->settings;
    }

    /**
     * Setup Configuration Object
     *
     * @return stdClass
     */
    protected function setGeneric()
    {
    	$generic = Filesystem::getInstance()->getScanDir(INI_PATH);
    	if(!empty($generic) && is_array($generic)){
	    	foreach ($generic as $items) {
    			if (!$items['type']) {
    				if (!is_array($this->settings)) {
    					$this->settings = [];
    				}
    				$file = INI_PATH . DIRECTORY_SEPARATOR . $items['item'];
    				if (!is_file($file)) {
    					throw ErrorException::showThrow(
    					    sprintf(
    					        'The general configuration ini-file "%s" does not exist',
                                $file
                            )
                        );
    				}
    				$ini = parse_ini_file($file,true);
    				if (!$ini) {
    					throw ErrorException::showThrow(
    						sprintf(
    						    'The general configuration ini-file "%s" does not exist or empty',
                                $file
                            )
    					);
    				}
    				foreach ($ini['production'] as $key => $item) {
    					$this->settings = array_merge_recursive(
    						$this->settings,
    						Arrays::getInstance()->items2MultiDimensionalKeys(
    							Arrays::getInstance()->explode($key,'.'),
    							$item
    						)
    					);
    				}
                    $this->settings = Arrays::getInstance()->arr2obj($this->settings);
    			}
    		}
    	}
    }

	/**
     * 
     * The handler functions that do not exist
     * 
     * @return Exeption, default - NULL
     * 
     */
	public function __call($method, $args) 
    {
    	if(!method_exists($this, $method)) { 
         	throw ErrorException::getInstance()->getMessage(
         		sprintf('The required method "%s" does not exist for %s', $method, get_class($this))
         	); 
       	} 	
    }
}