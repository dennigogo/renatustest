<?php

namespace RenatUsTest\Config;

use RenatUsTest\Helpers\Arrays;
use RenatUsTest\Db\Functions\Pdo;
use RenatUsTest\Throws\ErrorExceptions;

/**
 * Class Db
 * @package RenatUsTest\Config
 * @author Denis Gubenko
 */
class Db
{
	/**
     * Singleton instance
     * @var \RenatUsTest\Config\Db|null
     */
    private static $instance = null;
    
    /**
     * General variable
     * @var array
     */
    private $db = null;
    
    /**
     * Db constructor.
     */
	private function __construct(){

		return $this;
	}

	/**
     * Singleton pattern implementation makes "clone" unavailable
     * @return void
     */
    private function __clone(){}

    /**
     * Returns an instance of \RenatUsTest\Config\Db
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Config\Db Provides a fluent interface
     */
    public static function getInstance()
    {
    	if (null === self::$instance) {
            self::$instance = new self();
            self::$instance->setGeneric();
        }

        return self::$instance;
    }

    /**
     * Reset instance of \RenatUsTest\Config\Db
     * Singleton pattern implementation
     *
     * @return \RenatUsTest\Config\Db Provides a fluent interface
     */
    public static function resetInstance()
    {
        return self::$instance = null;
    }

    /**
     * Returns Configuration DB
     * @return array
     */
    public function getGeneric() :array
    {
    	if (!is_array($this->db)) {
    		self::setGeneric();
    	}

    	return $this->db;
    }

    /**
     * Setup Configuration Object
     *
     * @return void
     */
    protected function setGeneric() :void
    {
        $return = null;
        $database =
            (!empty(Kernel::getInstance()->getSettings()->db))
            ? Kernel::getInstance()->getSettings()->db : null;
        if ($database instanceof \stdClass) {
            foreach ($database as $driver => $items) {
                if ($items instanceof \stdClass) {
                    if (!is_array($this->db)) {
                        $this->db = [];
                    }
                    foreach ($items as $key => $item) {
                        if (empty($this->db[$key])) {
                            $class = '\\RenatUsTest\\Db\\Config\\' . ucfirst($driver);
                            $config = new $class(Arrays::getInstance()->obj2arr($items->$key,0), $key);
                            $class = '\\RenatUsTest\\Db\\Functions\\' . ucfirst($driver);
                            $functions = new $class($config, $key);
                            $this->db[$key] = $functions;
                        }
                    }
                }
            }
        }
    }

    /**
     * @param string $namespace
     * @return null|\RenatUsTest\Db\Functions\Pdo|\RenatUsTest\Db\Functions\Mongo
     */
    public function getConnection(string $namespace)
    {

        return (!empty($this->db[$namespace])) ? $this->db[$namespace] : null;
    }

	/**
     * The handler functions that do not exist
     * @return void
     */
	public function __call($method, $args) 
    {
    	if(!method_exists($this, $method)) { 
         	throw ErrorExceptions::showThrow(
                sprintf(
                    'The required method "%s" does not exist for %s',
                    $method,
                    get_class($this)
                )
            );
       	} 	
    }
}