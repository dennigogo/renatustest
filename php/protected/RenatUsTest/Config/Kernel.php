<?php

namespace RenatUsTest\Config;

use RenatUsTest\Throws\ErrorException;

/**
 * Class Kernel
 * @package RenatUsTest\Config
 * @author Denis Gubenko
 */
class Kernel
{
    /**
     * Singleton instance.
     * @var null|\RenatUsTest\Config\Kernel
     */
    protected static $instance = null;
    
    /**
     * Settings (current).
     *
     * @var null|\RenatUsTest\Config\Settings
     */
    protected $settings = null;
    
    /**
     * DB for Apps.
     *
     * @var null|stdClass
     */
    protected $db = null;
    
    /**
     * Singleton pattern implementation makes "new" unavailable.
     * @return void
     */
    protected function __construct(){

        return $this;
    }
    
    /**
     * Singleton pattern implementation makes "clone" unavailable.
     * @return void
     */
    protected function __clone(){}
    
    /**
     * Returns an instance of \RenatUsTest\Config\Kernel.
     * Singleton pattern implementation.
     * @return \RenatUsTest\Config\Kernel
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
            self::$instance->buildKernel();
        }

        return self::$instance;
    }
    
    /**
     * Build Kernel Object.
     * @return void
     */
    public function buildKernel()
    {
        self::setSettings(Settings::getInstance()->getGeneric());
        if (!empty($this->getSettings()->db) && $this->getSettings()->db instanceof \stdClass) {
            self::setDb($this->getSettings()->db);
        }
        Db::getInstance()->getGeneric();
    }

    /**
     * Set DB for apps.
     * @param null|stdClass
     * @return null|stdClass
     */
    protected function setDb($db = null)
    {
        return $this->db = $db;
    }
    
    /**
     * Get DB for apps.
     *
     * @return null|stdClass
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Set Settings.
     * 
     * @param stdClass|null $options
     * @return void
     */
    protected function setSettings($options = null)
    {
        if (empty($options)) {
            throw ErrorException::showThrow(
                'Critical error. Settings undefined.'
            );
        }

        $this->settings=$options;
    }
    
    /**
     * Get Settings.
     *
     * @return stdClass|mixed
     */
    public function getSettings()
    {
        if (empty($this->settings)) {
            throw ErrorException::showThrow(
                'Critical error. Settings undefined.
            ');
        }

        return $this->settings;
    }
    
    /**
     * The handler functions that do not exist.
     * @param string $method
     * @param mixed $args
     * @return void
     */
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
             throw ErrorException::getInstance()->getMessage(
                 sprintf(
                     'The required method "%s" does not exist for %s',
                     $method, get_class($this)
                 )
             );
        }
    }
}