<?php

defined('INI_PATH')
|| define(
    'INI_PATH',
    __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR
);

require_once './protected/vendor/autoload.php';
require_once './protected/RenatUsTest/autoload.php';