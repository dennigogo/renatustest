echo '====================='
echo ''
echo 'Hello!'
echo ''
echo 'Domain list:'
echo 'entry: http://nodejs10.int'
if [ ${INSTALL_NGINX} = true ]; then
    if [ ${INSTALL_PHP7} = true ]; then
        if [ ${INSTALL_MYSQL} = true ]; then
            echo "phpMyAdmin: http://mysql.nodejs10.int (user: 'root' , password: '$PASSWD_MYSQL')"
        fi
    fi
fi
if [ ${INSTALL_MONGODB} = true ]; then
    echo 'mongodb: http://nodejs10.int:3000 (server: localhost; db: admin; port 27017; not user, not password)'
    echo ''
    echo "MongoDb: not security; only localhost"
fi
if [ ${INSTALL_MYSQL} = true ]; then
    echo ''
    echo "Mysql: user - 'root'; pass - '$PASSWD_MYSQL'"
fi
if [ ${INSTALL_NODEJS10} = true ]; then
    echo ''
    echo 'For restart nodejs use command'
    echo '#sudo node.service {start|stop|restart} > /dev/null 2>&1'
    echo 'with "vagrant ssh"'
fi
echo ''
echo '====================='
