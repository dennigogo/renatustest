#!/usr/bin/env bash

# set locale 
# sudo update-locale LC_ALL=C.UTF-8
# Set non-interactive mode
export DEBIAN_FRONTEND=noninteractive


# abort on nonzero exitstatus
#set -o errexit
# abort on unbound variable
#set -o nounset
# don't mask errors in piped commands
#set -o pipefail

# Color definitions
readonly reset='\e[0m'
readonly cyan='\e[0;36m'
readonly red='\e[0;31m'
readonly yellow='\e[0;33m'	


info() {
 printf "${cyan}>>> %s${reset}\n" "${*}" 
}


if [ ! -e /home/dsearch/vagrant/.provision ];
then
	
    info "updating system packages"
    sudo apt-get update --fix-missing
    sudo apt-get --yes upgrade
    echo "."

    info "installing essentials and tools ..."
    sudo apt-get install --yes software-properties-common
    sudo apt-get install --yes python-software-properties
    sudo apt-get install --yes wget curl htop vim nano gcc g++ make
    sudo apt-get install --yes ssh
    sudo apt-get install --yes mc
    sudo apt-get install --yes git git-flow
    sudo apt-get install --yes docker.io
    echo "."

    #--------------------------------------
    # Nginx
    #--------------------------------------

    if [ ${INSTALL_NGINX} = true ]; then                
        info "Installing Nginx"
        sudo apt-get install --yes nginx
        
        echo "."
    fi
    

    #--------------------------------------
    # Php 7
    #--------------------------------------

    if [ ${INSTALL_PHP7} = true ]; then
        info "Installing php 7.1"
        sudo add-apt-repository ppa:ondrej/php
        sudo apt-get update > /dev/null
        sudo apt-get install -qq php7.1-cli php7.1-common php7.1-curl php7.1-fpm php7.1-gd php7.1-intl  php7.1-mbstring php7.1-mcrypt php7.1-mysql php7.1-opcache php7.1-xml php7.1-xmlrpc php7.1-zip php-imagick php7.1-dev php7.1-mongo
        sudo apt-get install --qq libcurl4-openssl-dev pkg-config libssl-dev libsslcommon2-dev        
        sudo php --ini
        echo "."

        info "installing Composer"
        cd /tmp
        sudo curl -sS https://getcomposer.org/installer | php
        sudo mv composer.phar /usr/local/bin/composer
        sudo composer global require "fxp/composer-asset-plugin:^1.2.0"    
    	cd /home/php
	composer update
        echo "."
		
        info "installing PHPUnit 6.4.4"
        cd /tmp
        wget https://phar.phpunit.de/phpunit-6.4.4.phar
        sudo chmod +x phpunit-6.4.4.phar
        sudo mv phpunit-6.4.4.phar /usr/local/bin/phpunit
        echo "."
		
        info "Installing XDebug"
        pecl install xdebug
        sudo echo "zend_extension=xdebug.so" > /etc/php/7.1/mods-available/xdebug.ini
        sudo ln -sf /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/fpm/conf.d/20-xdebug.ini
        sudo ln -sf /etc/php/7.1/mods-available/xdebug.ini /etc/php/7.1/cli/conf.d/20-xdebug.ini
        sudo service php-fpm restart
        echo "."
    fi

    #--------------------------------------
    # MySql
    #--------------------------------------

    if [ ${INSTALL_MYSQL} = true ]; then                
        info "Installing MySql"
        sudo apt-get --yes purge mysql-* && sudo apt-get --yes autoremove && sudo apt-get --yes autoclean
        sudo deluser --remove-home mysql && sudo delgroup mysql
        sudo rm -rf /etc/apparmor.d/abstractions/mysql /etc/apparmor.d/cache/usr.sbin.mysqld /etc/mysql /var/lib/mysql /var/log/mysql* /var/log/upstart/mysql.log* /var/run/mysqld ~/.mysql_history
        debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWD_MYSQL"
        debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWD_MYSQL"
        sudo apt-get install --yes mysql-server
        mysql -u ${PASSWD_MYSQL} -p${PASSWD_MYSQL} < /home/mysql/dump/tracker.sql
        if [ ${INSTALL_PHP7} = true ]; then
            if [ ${INSTALL_NGINX} = true ]; then               
                sudo mkdir /var/www/phpMyAdmin
                sudo composer create-project -s stable phpmyadmin/phpmyadmin /var/www/phpMyAdmin

                sudo chown -R www-data:www-data /var/www/phpMyAdmin
                sudo ln -sf /vagrant/vagrant/nginx/phpMyAdmin.conf /etc/nginx/sites-enabled/phpMyAdmin
                sudo systemctl restart nginx
                sudo systemctl status nginx
            fi
        fi
        
        echo "."
    fi

    #--------------------------------------
    # NodeJS 10
    #--------------------------------------

    if [ ${INSTALL_NODEJS10} = true ]; then
        info "Installing NodeJS10"
        sudo rm -ifr /tmp/node && mkdir -p /tmp/node
        cd /tmp/node
        wget https://nodejs.org/download/nightly/v10.0.0-nightly201804201b438a7737/node-v10.0.0-nightly201804201b438a7737-linux-x64.tar.gz
        tar -xvvzf *.tar.gz
        sudo rm -ifr /opt/node10 && sudo mv node-v10.0.0-nightly201804201b438a7737-linux-x64 /opt/node10
        sudo rm -ifr /usr/local/bin/node && sudo ln -s /opt/node10/bin/node /usr/local/bin/node
        sudo rm -ifr /usr/local/bin/npm && sudo ln -s /opt/node10/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm
        sudo rm -ifr /usr/local/bin/npx && sudo ln -s /opt/node10/lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx
        sudo rm -ifr /tmp/node
        sudo ln -sf /home/nodejs/init/start /usr/local/bin/node.service
        cd /home/nodejs/script/1100
        npm install

        echo "."
    fi  

    if [ ${INSTALL_MONGODB} = true ]; then
        info "installing MongoDB"
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
        echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
        sudo apt update -y
        
        sudo service mongod stop > /dev/null
        sudo apt-get --yes purge mongodb-org*
        sudo rm -rif /var/log/mongodb
        sudo rm -rif /var/lib/mongodb

        sudo apt install -y --force-yes mongodb-org        
        sudo systemctl enable mongod
        sudo systemctl start mongod
        sudo systemctl status mongod
        sudo service mongod restart
        sudo docker pull mongoclient/mongoclient
    fi

    #--------------------------------------
    # configure fixes, clean and done
    #--------------------------------------

    #configure locales
    export LANGUAGE=en_US.UTF-8
    export LANG=en_US.UTF-8
    export LC_ALL=en_US.UTF-8
    locale-gen en_US.UTF-8
    dpkg-reconfigure locales

    #clean up
    sudo apt-get autoremove > /dev/null
    sudo apt-get autoclean > /dev/null
    sudo apt-get clean > /dev/null

    touch /home/dsearch/vagrant/.provision
    info "Provisioning done."
fi