if [ ${INSTALL_NGINX} = true ]; then
    if [ ${INSTALL_PHP7} = true ]; then
        sudo cp -fr /vagrant/vagrant/php-fpm/php-fpm.service /etc/systemd/system
        sudo cp -fr /vagrant/vagrant/php-fpm/pool.d/www.conf /etc/php/7.1/fpm/pool.d
        sudo systemctl start php-fpm
        sudo systemctl status php-fpm
        sudo systemctl enable php-fpm
        if [ ${INSTALL_MYSQL} = true ]; then
            sudo chown -R www-data:www-data /var/www/phpMyAdmin
            sudo ln -sf /vagrant/vagrant/nginx/phpMyAdmin.conf /etc/nginx/sites-enabled/phpMyAdmin
        fi
        service php-fpm restart
    fi
    service nginx restart
fi
if [ ${INSTALL_NODEJS10} = true ]; then
    sudo rm -fr /usr/local/bin/node.service && sudo ln -s /home/nodejs/init/start /usr/local/bin/node.service
    sudo node.service restart > /dev/null 2>&1
    sudo rm -fr /etc/nginx/sites-enabled/nodejs10.conf && sudo ln -s /vagrant/vagrant/vhosts/nodejs10.conf /etc/nginx/sites-enabled/nodejs10.conf
    service nginx restart
fi
if [ ${INSTALL_MONGODB} = true ]; then
    sudo docker rm -f $(sudo docker ps -aq) >/dev/null 2>&1 || true
    docker run -d -p 3000:3000 mongoclient/mongoclient
fi